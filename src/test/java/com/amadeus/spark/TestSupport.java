package com.amadeus.spark;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestSupport {
	
	public String getGroup(String input, String regex, int group) {
		return getGroups(input, regex, group)[0];
	}
	
	public String[] getGroups(String input, String regex, int...groups) {
		String[] results = new String[groups.length];
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(input);
		boolean matches = m.matches();
		if(matches){
			for(int i=0; i<results.length; i++) {
				results[i] = m.group(groups[i]);
			}
		}
		return results;
	}
}
