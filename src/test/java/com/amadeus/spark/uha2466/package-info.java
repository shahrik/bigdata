/**
 * This package containst the testNG scenarios 
 * for UHA-2466-Event for CTRLs.
 * @author Kambiz.Shahri
 *
 */
package com.amadeus.spark.uha2466;