/**
 * This package contains the testNG scenarios 
 * for UHA-2465-Event for RTOs.
 * @author Kambiz.Shahri
 *
 */
package com.amadeus.spark.uha2465;