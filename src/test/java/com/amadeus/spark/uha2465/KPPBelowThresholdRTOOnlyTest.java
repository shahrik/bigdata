package com.amadeus.spark.uha2465;

import java.time.Instant;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.amadeus.spark.config.CircuitBreakerConfig;
import com.amadeus.spark.sla.NotificationServiceKafkaMetricPublisher;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagSla;

/**
 * We want to simulate circuit.breaker SLA events like RTO for our Use Case.
 * 
 * @author kambiz.shahri
 *
 */
public class KPPBelowThresholdRTOOnlyTest {

   @SuppressWarnings("unused")
   private static final Logger log = LoggerFactory
         .getLogger(KPPBelowThresholdRTOOnlyTest.class);

   @Test
   public void f() {

      MessagePublish mp1 = new MessagePublish(AnalysisMetric.MetricTagType.RTO, 50, "SANTA CLARA", "720", "BOOKINGS", 4);
      Thread t = new Thread(mp1);
      t.start();
      
      try {
         t.join();
        Assert.assertTrue(true);
      } catch (InterruptedException e) {
         e.printStackTrace();
        Assert.assertTrue(false);  
      }
   }

   /**
    * Publish an SLA.
    * 
    * @author kambiz.shahri
    *
    */
   class MessagePublish implements Runnable {

      private final Logger log = LoggerFactory.getLogger(MessagePublish.class);

      NotificationServiceKafkaMetricPublisher notificationServiceCircuitBreaker;

      //DEFAULT VALUES
      long INITIAL_SLEEP_INTERVAL = 5000;
      long SUBSEQUENT_SLEEP_INTERVAL = 250;
      String DATA_CENTER_ID = "1";
      String ZONE_ID = "720";
      String SERVICE_ID = "BOOKINGS";
      AnalysisMetric.MetricTagType ANAL_TAG_TYPE;
      String ANAL_DESCRIPTION = "Event::"+ANAL_TAG_TYPE+" SLA Breache Simulation from TestNG Driver.";
      int TOTAL_MESSAGES_TO_PUBLISH = 5;
      MetricTagSla ANAL_SLA_OK_KO = MetricTagSla.SLA_KO; //can never be SLA_OK...not possible since we are listening in on an SLA Breach topic.
      
      /*
       * Constructor to init the above to other values.
       */
      MessagePublish(AnalysisMetric.MetricTagType SLAType, long intervalToUse, String dataCenterId, String zoneId, String serviceId, int totalMessagesToPublish) {
         this.ANAL_TAG_TYPE = SLAType;
         this.SUBSEQUENT_SLEEP_INTERVAL = intervalToUse;
         DATA_CENTER_ID = (dataCenterId == null || dataCenterId.length()==0) ? "1" :  dataCenterId;
         ZONE_ID = (zoneId == null || zoneId.length()==0) ? "720" :  zoneId;
         SERVICE_ID = (serviceId == null || serviceId.length()==0) ? "BOOKINGS" : serviceId;
         TOTAL_MESSAGES_TO_PUBLISH = (totalMessagesToPublish<=0) ? 5 : totalMessagesToPublish;
         
      }
      /* (non-Javadoc)
       * @see java.lang.Runnable#run()
       */
      @Override
      public void run() {
         ApplicationContext ctx = new AnnotationConfigApplicationContext(
               CircuitBreakerConfig.class);
        Assert.assertNotNull(ctx);

         System.out.println(Arrays.deepToString(ctx.getBeanDefinitionNames()));
         log.info(Arrays.deepToString(ctx.getBeanDefinitionNames()).contains(
               "slaBreachMetricsNotifier") ? "FOUND slaBreachMetricsNotifier"
                     : "NOT FOUND!!! slaBreachMetricsNotifier");
        Assert.assertTrue(Arrays.deepToString(ctx.getBeanDefinitionNames()).contains("slaBreachMetricsNotifier"));

         notificationServiceCircuitBreaker = (NotificationServiceKafkaMetricPublisher) ctx
               .getBean("slaBreachMetricsNotifier");
        Assert.assertNotNull(notificationServiceCircuitBreaker);

         double average = 100;
         long timestamp=0;
         try {
            Thread.sleep(INITIAL_SLEEP_INTERVAL);
           Assert.assertTrue(true);
         } catch (InterruptedException e1) {
            e1.printStackTrace();
           Assert.assertTrue(false);
         }
         for (int i = 0; i < TOTAL_MESSAGES_TO_PUBLISH; i++) {
            // Create the event for the notification
            timestamp = Instant.now().getEpochSecond();
            try {
               notificationServiceCircuitBreaker
                     .publishMetric(new AnalysisMetric(
                           timestamp, 
                           average,
                           AnalysisMetric.MetricTagWhat.SLA,
                           ANAL_TAG_TYPE,
                           ANAL_SLA_OK_KO, 
                           DATA_CENTER_ID, 
                           ZONE_ID, 
                           "", //siloId
                           "", //server
                           SERVICE_ID,
                           ANAL_DESCRIPTION));
              Assert.assertTrue(true);
            } catch (Exception e) {
               e.printStackTrace();
              Assert.assertTrue(false);
            }
            try {
               Thread.sleep(SUBSEQUENT_SLEEP_INTERVAL);
              Assert.assertTrue(true);
            } catch (InterruptedException e) {
               e.printStackTrace();
              Assert.assertTrue(false);
            }
         }
      }
   }
}
