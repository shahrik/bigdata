package com.amadeus.spark.collectors;

import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.amadeus.spark.SparkTestBase;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;

public class AverageDurationCollectorTest extends SparkTestBase {
	
	MetricsCollectorAverageDuration collector;
	
	SparkSubjectUnderTest<JavaPairDStream<String,String>> sut = new SparkSubjectUnderTest<JavaPairDStream<String,String>>() {
		@Override
		public void call(JavaPairDStream<String, String> input) {
			collector.collect(input);
		}
	};
	
	@Before
	public void setUp() {
      String[] fieldsToAssertAnalysisMetrics = { "metricTagWhat", "type",
            "value", "metricTagSla", "zoneId", "siloId", "serviceName" };
      this.fieldsToAssertAnalysisMetrics = fieldsToAssertAnalysisMetrics;

		}
	
	@Test
	public void shouldNotifyTwoAverageDurationsWhenThereAreTwoHosts() {
      LogEventKeyField[] logEventKeysGrouping = {
            LogEventKey.LogEventKeyField.ISOLATION_ZONE,
            LogEventKey.LogEventKeyField.SILO };

      collector = new MetricsCollectorAverageDuration(logEventKeysGrouping);

      ReflectionTestUtils.setField(collector, "sparkOperations",
            sparkOperations);
      ReflectionTestUtils.setField(collector, "notificationService",
            notificationService);

		String inputFile = "avgduration.collector.test.four.slow.events.for.two.hosts.in";
		String outputFile = "avgduration.collector.test.four.slow.events.for.two.hosts.out";
		
		assertData(inputFile, outputFile, sut);
	}

   @Test
   public void shouldNotifyTwoAverageDurationsWhenGroupedByService() {
      LogEventKeyField[] logEventKeysGrouping = {
            LogEventKey.LogEventKeyField.DATACENTER,
            LogEventKey.LogEventKeyField.ISOLATION_ZONE,
            LogEventKey.LogEventKeyField.SERVICE_NAME };

      collector = new MetricsCollectorAverageDuration(logEventKeysGrouping);

      ReflectionTestUtils.setField(collector, "sparkOperations",
            sparkOperations);
      ReflectionTestUtils.setField(collector, "notificationService",
            notificationService);

      String inputFile = "avgduration.collector.test.group.by.servicename.in";
      String outputFile = "avgduration.collector.test.group.by.servicename.out";

      assertData(inputFile, outputFile, sut);
   }

}