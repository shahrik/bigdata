package com.amadeus.spark.collectors;

import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.amadeus.spark.SparkTestBase;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;

public class ResponseTimeCollectorTest extends SparkTestBase {
	
	MetricsCollectorResponseTimeBreachPercentage collector;
	
	SparkSubjectUnderTest<JavaPairDStream<String,String>> sut = new SparkSubjectUnderTest<JavaPairDStream<String,String>>() {
		@Override
		public void call(JavaPairDStream<String, String> input) {
			collector.collect(input);
		}
	};
	
	@Before
	public void setUp() {	   
	   
	}
	
	@Test
	public void shouldNotifyWhen() {

	   LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE, LogEventKeyField.SERVICE_NAME};
      collector = new MetricsCollectorResponseTimeBreachPercentage(groupingFields, 0.12);
      
      ReflectionTestUtils.setField(collector, "sparkOperations", sparkOperations);
      ReflectionTestUtils.setField(collector, "notificationService", notificationService);
      
		String inputFile = "resptime.collector.test.four.slow.events.in";
		String outputFile = "resptime.collector.test.four.slow.events.out";
		
		assertData(inputFile, outputFile, sut);
	}
	
	@Test
	public void shouldNotify80PercentWhenFourOutOfFiveExceedThresholdForTwoHosts() {
	   LogEventKeyField[] groupingFields = {
            LogEventKey.LogEventKeyField.ISOLATION_ZONE,
            LogEventKey.LogEventKeyField.SILO };

	   collector = new MetricsCollectorResponseTimeBreachPercentage(groupingFields, 0.12);
      
      ReflectionTestUtils.setField(collector, "sparkOperations", sparkOperations);
      ReflectionTestUtils.setField(collector, "notificationService", notificationService);
      
		String inputFile = "resptime.collector.test.four.slow.events.for.two.hosts.in";
		String outputFile = "resptime.collector.test.four.slow.events.for.two.hosts.out";
		
		assertData(inputFile, outputFile, sut);
	}
}