package com.amadeus.spark.collectors;

import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.amadeus.spark.SparkTestBase;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;

public class ThroughputCollectorTest extends SparkTestBase {
	
	MetricsCollectorThroughput collector;
	
	SparkSubjectUnderTest<JavaPairDStream<String,String>> sut = new SparkSubjectUnderTest<JavaPairDStream<String,String>>() {
		@Override
		public void call(JavaPairDStream<String, String> input) {
			collector.collect(input);
		}
	};
	
	@Before
	public void setUp() {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE, LogEventKeyField.SERVICE_NAME};
      collector = new MetricsCollectorThroughput(groupingFields);
		
      ReflectionTestUtils.setField(collector, "sparkOperations",
            sparkOperations);
      ReflectionTestUtils.setField(collector, "notificationService",
            notificationService);
	}
	
	@Test
   public void shouldCountMessagesSplitByTwoServices() {
		String inputFile = "throughput.collector.events.in";
		String outputFile = "throughput.collector.events.out";
		
		assertData(inputFile, outputFile, sut);
	}
   
   @Test
   public void shouldCountMessagesSplitByDataCenterZoneIdServicesName() {
      String inputFile = "throughput.collector.events.grouping.in";
      String outputFile = "throughput.collector.events.grouping.out";

      assertData(inputFile, outputFile, sut);
   }

}