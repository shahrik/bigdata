package com.amadeus.spark.collectors;

import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.amadeus.spark.SparkTestBase;

public class LqsMessageCollectorTest extends SparkTestBase {
	
	LqsCollectorMessage collector;
	
	SparkSubjectUnderTest<JavaPairDStream<String,String>> sut = new SparkSubjectUnderTest<JavaPairDStream<String,String>>() {
		@Override
		public void call(JavaPairDStream<String, String> input) {
			collector.collect(input);
		}
	};
	
	@Before
	public void setUp() {
      String[] fieldsToAssertAnalysisMetrics = { "metricTagWhat", "type",
            "value", "metricTagSla", "queueId", "queueName" };
      this.fieldsToAssertAnalysisMetrics = fieldsToAssertAnalysisMetrics;

		}
	
	@Test
	public void shouldSeparateCountQueueItemsPerQueue() {

      collector = new LqsCollectorMessage();

      ReflectionTestUtils.setField(collector, "sparkOperations",
            sparkOperations);
      ReflectionTestUtils.setField(collector, "notificationService",
            notificationService);

		String inputFile = "lqs.collector.events.itemcount.in";
		String outputFile = "lqs.collector.events.itemcount.out";
		
		assertData(inputFile, outputFile, sut);
	}

}