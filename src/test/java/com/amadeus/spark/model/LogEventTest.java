package com.amadeus.spark.model;

import org.junit.Assert;
import org.junit.Test;

public class LogEventTest {
   
   @Test
   public void checkJsonCreationLogEventKey() {
      LogEvent logEvent = new LogEvent();
      LogEventHeader logEventHeader = new LogEventHeader();
      LogEventContent logEventContent = new LogEventContent();
      logEventHeader.setkIndexKey_HostName("hostnameValue.01");
      logEventContent.setkIndexKey_DataCenter("datacenter.01");
      logEventContent.setkIndexKey_IsolationZone("zone.01");
      logEventContent.setkIndexKey_Silo("silo.01");
      logEventContent.setkIndexKey_Sap("sap.01");
      logEvent.setHeader(logEventHeader);
      logEvent.setContent(logEventContent);
      LogEventKey logEventKeyExpected = new LogEventKey(logEvent);
      LogEventKey logEventKeyActual = LogEventKey.toObject(logEventKeyExpected.toJsonOnlyIncludeFields(LogEventKey.LogEventKeyField.ISOLATION_ZONE, LogEventKey.LogEventKeyField.SILO));
      System.out.println(logEventKeyExpected.toJsonOnlyIncludeFields(LogEventKey.LogEventKeyField.ISOLATION_ZONE, LogEventKey.LogEventKeyField.SILO));
      System.out.println(logEventKeyActual.toJsonOnlyIncludeFields(LogEventKey.LogEventKeyField.ISOLATION_ZONE, LogEventKey.LogEventKeyField.SILO));
      Assert.assertEquals(logEventKeyExpected.toJsonOnlyIncludeFields(LogEventKey.LogEventKeyField.ISOLATION_ZONE, LogEventKey.LogEventKeyField.SILO), logEventKeyActual.toJsonOnlyIncludeFields(LogEventKey.LogEventKeyField.ISOLATION_ZONE, LogEventKey.LogEventKeyField.SILO));
      
   }
   
   @Test
   public void checkJsonCreationLogEventKeyIncludingOnlyCertainFields() {
      LogEvent logEvent = new LogEvent();
      LogEventHeader logEventHeader = new LogEventHeader();
      LogEventContent logEventContent = new LogEventContent();
      logEventHeader.setkIndexKey_HostName("hostnameValue.01");
      logEventContent.setkIndexKey_DataCenter("datacenter.01");
      logEventContent.setkIndexKey_IsolationZone("zone.01");
      logEventContent.setkIndexKey_Silo("silo.01");
      logEventContent.setkIndexKey_Sap("sap.01");
      logEvent.setHeader(logEventHeader);
      logEvent.setContent(logEventContent);
      LogEventKey logEventKeyActual = new LogEventKey(logEvent);
      System.out.println(logEventKeyActual.toJsonOnlyIncludeFields(LogEventKey.LogEventKeyField.ISOLATION_ZONE, LogEventKey.LogEventKeyField.SILO));
      Assert.assertEquals("{\"zoneId\":\"zone.01\",\"siloId\":\"silo.01\"}", logEventKeyActual.toJsonOnlyIncludeFields(LogEventKey.LogEventKeyField.ISOLATION_ZONE, LogEventKey.LogEventKeyField.SILO));
      
   }
   
   @Test
   public void checkJsonCreationLogEventKeyIncludingOnlyCertainFieldsDatacenterIsolationzoneService() {
      LogEvent logEvent = new LogEvent();
      LogEventHeader logEventHeader = new LogEventHeader();
      LogEventContent logEventContent = new LogEventContent();
      logEventHeader.setkIndexKey_HostName("hostnameValue.01");
      logEventContent.setkIndexKey_DataCenter("datacenter.01");
      logEventContent.setkIndexKey_IsolationZone("zone.01");
      logEventContent.setkIndexKey_Silo("silo.01");
      logEventContent.setkIndexKey_Sap("sap.01");
      logEventContent.setkIndexKey_ServiceName("service.01");
      logEvent.setHeader(logEventHeader);
      logEvent.setContent(logEventContent);
      LogEventKey logEventKeyActual = new LogEventKey(logEvent);
      System.out.println(logEventKeyActual.toJsonOnlyIncludeFields(LogEventKey.LogEventKeyField.DATACENTER, LogEventKey.LogEventKeyField.ISOLATION_ZONE, LogEventKey.LogEventKeyField.SERVICE_NAME));
      Assert.assertEquals("{\"dataCenter\":\"datacenter.01\",\"zoneId\":\"zone.01\",\"serviceName\":\"service.01\"}", logEventKeyActual.toJsonOnlyIncludeFields(LogEventKey.LogEventKeyField.DATACENTER, LogEventKey.LogEventKeyField.ISOLATION_ZONE, LogEventKey.LogEventKeyField.SERVICE_NAME));
      
   }



}
