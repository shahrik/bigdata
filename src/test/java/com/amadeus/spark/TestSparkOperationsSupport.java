package com.amadeus.spark;


import java.io.Serializable;

import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;

import scala.Tuple2;

public class TestSparkOperationsSupport implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public JavaPairDStream<String, String> toJavaPairDStream(JavaDStream<String> lines) {
		return lines.mapToPair(new PairFunction<String, String, String>() {
			private static final long serialVersionUID = 1L;
			@Override
			public Tuple2<String, String> call(String t) throws Exception {
				return new Tuple2<String, String>(t,t);
			}
		});
	}
}