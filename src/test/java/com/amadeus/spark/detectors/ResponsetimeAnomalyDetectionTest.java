package com.amadeus.spark.detectors;

import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import com.amadeus.spark.SparkTestBase;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;

public class ResponsetimeAnomalyDetectionTest extends SparkTestBase {
	
	ResponsetimeAnomalyDetection detector;
	
	SparkSubjectUnderTest<JavaPairDStream<String,String>> sut = new SparkSubjectUnderTest<JavaPairDStream<String,String>>() {
		@Override
		public void call(JavaPairDStream<String, String> input) {
			detector.detect(input);
		}
	};
	
	@Before
	public void setUp() {
	   LogEventKeyField[] groupingFields = {
            LogEventKey.LogEventKeyField.DATACENTER,
            LogEventKey.LogEventKeyField.ISOLATION_ZONE,
            LogEventKey.LogEventKeyField.SERVICE_NAME };
		detector = new ResponsetimeAnomalyDetection(groupingFields, 0.12,5);
		
		ReflectionTestUtils.setField(detector, "sparkOperations", sparkOperations);
		ReflectionTestUtils.setField(detector, "slaBreachNotificationService", notificationService);
	}
	
	@Test
	public void shouldGenerateSlaBreachNotificationWhenOver5Percent() {
		String inputFile = "resptime.detector.test.four.slow.events.for.two.hosts.in";
		String outputFile = "resptime.detector.test.four.slow.events.for.two.hosts.out";
		
		assertData(inputFile, outputFile, sut);
	}
}