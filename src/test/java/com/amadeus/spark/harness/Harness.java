package com.amadeus.spark.harness;

/**
 * Test harness driver.
 * @author Kambiz.Shahri
 *
 */
public class Harness {

   /**
    * @param args
    */
   public static void main(String[] args) {
      
      Thread t = new Thread(new Runnable () {
         @Override
         public void run() {
            System.out.println("Running Event4RTOTest");
        /*    TestListenerAdapter tla = new TestListenerAdapter();
            TestNG testng = new TestNG();
            testng.setTestClasses(new Class[] { MetricsCollectorRTOSLABreach.class });
            testng.addListener(tla);
            testng.run();*/
         }
      });
      
      Thread t2 = new Thread(new Runnable () {
         @Override
         public void run() {
            System.out.println("Running ESMParameterizedPublisherTest");
            /*TestListenerAdapter tla = new TestListenerAdapter();
            TestNG testng = new TestNG();
            testng.setTestClasses(new Class[] { KafkaParameterizedPublisherExactlyAtThresholdRTOOnlyTest.class });
            //Add more cases here to be executed.
            testng.setTestClasses(new Class[] { KPPBelowThresholdRTOOnlyTest.class });
            testng.setTestClasses(new Class[] { KPPBelowThresholdRTOAndCTRLMixTest.class });
            testng.addListener(tla);
            testng.run();*/
         }
      });
      t.start();
      
      t2.start();
      try {
         t2.join();
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      try {
         t.join();
      } catch (InterruptedException e) {
         e.printStackTrace();
      }
      
   }
}
