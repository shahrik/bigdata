/**
 * TestNG harness that can be reused for running all
 * scenarios.
 * @author kambiz.shahri
 *
 */
package com.amadeus.spark.harness;