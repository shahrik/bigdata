package com.amadeus.spark;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.amadeus.spark.sla.NotificationService;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.EventMetric;
import com.amadeus.spark.utils.SparkOperations;
import com.google.common.io.Files;

public abstract class SparkTestBase {
	
	protected TestSparkOperationsSupport testSparkOperations = new TestSparkOperationsSupport();
	protected TestNotificationService notificationService = new TestNotificationService();
	protected SparkOperations sparkOperations = new SparkOperations();
	protected List<String> actualOutput = new ArrayList<String>();
	protected List<String> expectedOutput = null;
	protected TestSupport testSupport = new TestSupport();
	protected JavaStreamingContext sparkContext;
	protected long duration = 1000;
	protected File inputDataSource;
	
   private static final Logger log = LoggerFactory
         .getLogger(SparkTestBase.class);
	
   protected String[] fieldsToAssertAnalysisMetrics = { "metricTagWhat",
         "type", "value", "metricTagSla", "zoneId", "siloId", "serviceName", "dataCenter"};
   /**
   * @param inputFile
   * @param outputFile
   * @param sut
   */
   protected void assertData(String inputFile, String outputFile,
         SparkSubjectUnderTest<JavaPairDStream<String, String>> sut) {
		
	   SparkConf conf = new SparkConf();
	   conf.set("spark.master","local[2]");
	   conf.set("spark.app.name", sut.getClass().getSimpleName());
	   conf.set("spark.driver.allowMultipleContexts", "true");
	   
	   sparkContext = new JavaStreamingContext(conf, new Duration(duration));

	   try {
			
			inputDataSource = Files.createTempDir();

			JavaPairDStream<String, String> input = getInputDStream();

			sut.call(input);

			sparkContext.start();
			
			sleep(duration);//wait for spark to startup
			
			writeTestDataToFile(inputFile);

			waitForWorkToComplete();

			initExpectedOutput(outputFile);
			
			assertLines(expectedOutput, actualOutput);

			sparkContext.stop();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
            if (inputDataSource != null)
               FileUtils.deleteDirectory(inputDataSource);
				sparkContext.stop();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param inputFile
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	private void writeTestDataToFile(String inputFile)
			throws FileNotFoundException, UnsupportedEncodingException {
      File inputDataSourceFile = new File(inputDataSource.getAbsolutePath(),
            inputFile);
		PrintWriter writer = new PrintWriter(inputDataSourceFile, "UTF-8");
		List<String> testData = getTestData(inputFile);
		for (String line : testData) {
			writer.println(line);
		}
		writer.close();
	}

	/**
	 * 
	 */
	private void waitForWorkToComplete() {
		int maxAttempts = 10;
		int attempts = 0;
		while (!notificationService.isCalled()) {
			if(attempts >= maxAttempts) {
            log.info("We reached the max number of attempts " + maxAttempts
                  + " before the notification service gets called");
			   break;
			}
         log.info("About to sleep for " + duration
               + "ms, while waiting for the notification service to get called");
			sleep(duration);
			attempts++;
		}
	}

	/**
	 * @param outputFile
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private void initExpectedOutput(String outputFile)
			throws URISyntaxException, IOException {
      Path filePath = Paths.get(ClassLoader.getSystemResource(outputFile)
            .toURI());
      expectedOutput = java.nio.file.Files.readAllLines(filePath,
            Charset.forName("UTF8"));
      if (expectedOutput.size() == 0)
         throw new RuntimeException("Found no expected data to use for test.");
   }
/**
* @param inputLines
* @return
*/
   private void assertLines(List<String> expectedLines, List<String> actualLines) {
		for(String expected: expectedLines){
			StringBuilder sb = new StringBuilder("No data was found for assert.");
			boolean asserted = false;

			for(String actual: actualLines){
            sb.append("\n Lines did not match,   Expected: ").append(expected)
            .append("   Actual:").append(actual);
            AnalysisMetric actualAnalysisMetric = AnalysisMetric
                  .buildAnalysisMetric(actual);
            AnalysisMetric expectedAnalysisMetric = AnalysisMetric
                  .buildAnalysisMetric(expected);
            try {
               asserted = compareAnalysisMetricProperties(actualAnalysisMetric,
                     expectedAnalysisMetric, fieldsToAssertAnalysisMetrics, sb);
            } catch (Exception e) {
               throw new RuntimeException(e);
            }
            if (asserted)
               break;
			}
			Assert.assertTrue(sb.toString(), asserted);
		}
   }

   private boolean compareAnalysisMetricProperties(AnalysisMetric actual,
         AnalysisMetric expected, String[] beanPropertiesToCompare, StringBuilder sb)
         throws IllegalAccessException, IllegalArgumentException,
         InvocationTargetException {
      log.info("Asserting: " + String.join(", ", beanPropertiesToCompare));
      if (actual != null && expected != null) {
         for (String propertyName : beanPropertiesToCompare) {
            String methodName = "get"
                  + capitalizeFirstLetterString(propertyName);
            Method method = BeanUtils.findMethod(AnalysisMetric.class,
                  methodName);
            if (method == null) {
               throw new RuntimeErrorException(new Error(
                     "Problems finding the method: " + methodName));
            }
            Object actualPropertyValue = method.invoke(actual);
            Object expectedPropertyValue = method.invoke(expected);
            if (!(actualPropertyValue == expectedPropertyValue)) {
               if ((actualPropertyValue == null) || (expectedPropertyValue == null)){
                  sb.append("NOT MATCHING " + propertyName + " expected :"
                        + expectedPropertyValue + " actual:" + actualPropertyValue); 
                  return false;
               }
               if (!expectedPropertyValue.equals(actualPropertyValue)) {
                  if (!actualPropertyValue.toString().equals(expectedPropertyValue.toString())) {
                     sb.append("NOT MATCHING " + propertyName + " expected :"
                           + expectedPropertyValue.toString() + " actual:" + actualPropertyValue.toString());
                     return false;
                  }
               }
            }
         }
      }
      return true;
   }

   private String capitalizeFirstLetterString(String input) {
      return input.substring(0, 1).toUpperCase() + input.substring(1);
	}

	protected List<String> getTestData(String fileName) {
		List<String> lines = null;
		try {
         Path filePath = Paths.get(ClassLoader.getSystemResource(fileName)
               .toURI());
         lines = java.nio.file.Files.readAllLines(filePath,
               Charset.forName("UTF8"));
			return lines;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	protected JavaPairDStream<String, String> getInputDStream() {
      JavaDStream<String> lines = sparkContext.textFileStream(inputDataSource
            .getAbsolutePath());
      JavaPairDStream<String, String> input = testSparkOperations
            .toJavaPairDStream(lines);
		return input;
	}
	
	protected void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 *
	 */
	class TestNotificationService implements NotificationService {
		boolean isCalled = false;

		@Override
		public boolean publishMetric(EventMetric metric) throws Exception {
         log.info(metric.toJson());
			actualOutput.add(metric.toJson());
			return isCalled = true;
		}

		public boolean isCalled() {
			return isCalled;
		}
	}
	
	/**
	 *
	 * @param <T>
	 */
	@FunctionalInterface
	protected interface SparkSubjectUnderTest<T> {
		void call(T input);
	}
}