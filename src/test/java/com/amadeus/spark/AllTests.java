package com.amadeus.spark;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.amadeus.spark.collectors.AverageDurationCollectorTest;
import com.amadeus.spark.collectors.ResponseTimeCollectorTest;
import com.amadeus.spark.collectors.ThroughputCollectorTest;
import com.amadeus.spark.detectors.ResponsetimeAnomalyDetectionTest;


@RunWith(Suite.class)
@SuiteClasses({ResponseTimeCollectorTest.class, 
				AverageDurationCollectorTest.class,
				ResponsetimeAnomalyDetectionTest.class,
				ThroughputCollectorTest.class})
public class AllTests {}
