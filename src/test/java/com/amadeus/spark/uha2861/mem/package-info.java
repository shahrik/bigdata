/**
 * UHA-2861 VM Stats Memory test cases.
 * @author Kambiz.Shahri
 *
 */
package com.amadeus.spark.uha2861.mem;