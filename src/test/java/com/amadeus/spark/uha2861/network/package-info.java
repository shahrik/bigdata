/**
 * UHA-2861 VM Stats Network test cases.
 * @author Kambiz.Shahri
 *
 */
package com.amadeus.spark.uha2861.network;