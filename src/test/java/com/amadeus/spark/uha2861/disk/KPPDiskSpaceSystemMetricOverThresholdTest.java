package com.amadeus.spark.uha2861.disk;

import java.time.Instant;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amadeus.spark.uha2861.KafkaProducerUtility;

/**
 * We want to simulate circuit.breaker SLA events like RTO for our Use Case.
 * 
 * @author kambiz.shahri example record from current diamond output
 *         {"timestamp": 1446041354, "value": 75.5, "tags": {"path":
 *         "servers.ose3-int-a-master.cpu.total.idle", "what": "cpu", "type":
 *         "total.idle", "target_type": "gauge", "server": "ose3-int-a-master"}}
 *
 */
public class KPPDiskSpaceSystemMetricOverThresholdTest {

   @SuppressWarnings("unused")
   private static final Logger log = LoggerFactory
         .getLogger(KPPDiskSpaceSystemMetricOverThresholdTest.class);
   /**
    * Test to generate disk space messages.
    */
   @Test
   public void f() {
      String outputMessage = "{\"timestamp\": %s, \"value\": 2.58553809191149, \"tags\": {\"path\": \"servers.ose3-int-a-node1027.diskspace.root.byte_percentfree\", \"what\": \"diskspace\", \"type\": \"root.byte_percentfree\", \"target_type\": \"gauge\", \"server\": \"ose3-int-a-node1027\"}}";
      MessagePublish mp1 = new MessagePublish(outputMessage);
      Thread t = new Thread(mp1);
      t.start();
      try {
         t.join();
         Assert.assertTrue(true);
      } catch (InterruptedException e) {
         e.printStackTrace();
         Assert.assertTrue(false);
      }
   }

   /**
    * Publish an SLA.
    * @author kambiz.shahri
    */
   class MessagePublish implements Runnable {

      // DEFAULT VALUES
      long INITIAL_SLEEP_INTERVAL = 5000;
      long SUBSEQUENT_SLEEP_INTERVAL = 250;
      String DATA_CENTER_ID = "1";
      String ZONE_ID = "720";
      String SERVICE_ID = "BOOKINGS";
      int TOTAL_MESSAGES_TO_PUBLISH = 5;
      String messageFormat;

      /**
       * @param outgoingMessage
       */
      public MessagePublish(String incomingMessageFormat) {
         this.messageFormat = incomingMessageFormat;
      }

      /*
       * (non-Javadoc)
       * 
       * @see java.lang.Runnable#run()
       */
      @Override
      public void run() {
         try {
            Thread.sleep(INITIAL_SLEEP_INTERVAL);
            Assert.assertTrue(true);
         } catch (InterruptedException e1) {
            e1.printStackTrace();
            Assert.assertTrue(false);
         }
         for (int i = 0; i < TOTAL_MESSAGES_TO_PUBLISH; i++) {
            long timestamp = Instant.now().getEpochSecond();
            String outgoingMessage = String.format(messageFormat, timestamp);
            //Publish to Kafka
            KafkaProducerUtility.publishMessage(outgoingMessage);
            try {
               Thread.sleep(SUBSEQUENT_SLEEP_INTERVAL);
               Assert.assertTrue(true);
            } catch (InterruptedException e) {
               e.printStackTrace();
               Assert.assertTrue(false);
            }
         }
      }
   }
}
