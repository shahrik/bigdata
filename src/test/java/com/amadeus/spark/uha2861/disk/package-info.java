/**
 * UHA-2861 VM Stats Disk test cases.
 * @author Kambiz.Shahri
 *
 */
package com.amadeus.spark.uha2861.disk;