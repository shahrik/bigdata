package com.amadeus.spark.uha2861;

import org.junit.Assert;
import org.junit.Test;

import com.amadeus.spark.functions.FunctionsLibHelper;

public class FunctionsLibrarySystemMetricVMKeyParserTest {

   /**
    * Valid input. 1 key.
    */
   @Test
   public void testParseVMKeyValidInput() {
      String v2 = "{\"timestamp\": 1446041354, \"value\": 0.2, \"tags\": {\"path\": \"servers.kafka02.cpu.cpu0.irq\", \"what\": \"cpu\", \"type\": \"cpu0.irq\", \"target_type\": \"gauge\", \"server\": \"kafka02\"}}";
      try {
         Assert.assertEquals("servers.kafka02", FunctionsLibHelper.parseVMKey(v2));
         
      } catch (Exception e) {
         e.printStackTrace();
         Assert.assertTrue(false);
      }
   }
   
   /**
    * Invalid input no key.
    */
   @Test
   public void testParseVMKeyInValidInputNoKeyAtAll() {
      String v2 = "{\"timestamp\": 1446041354, \"value\": 0.2, \"tags\": {\"path\": \".cpu.cpu0.irq\", \"what\": \"cpu\", \"type\": \"cpu0.irq\", \"target_type\": \"gauge\", \"server\": \"kafka02\"}}";
      try {
         FunctionsLibHelper.parseVMKey(v2);
         Assert.assertTrue(false);
      } catch (Exception e) {
         e.printStackTrace();
         Assert.assertTrue(true);
      }
   }
   
   /**
    * InValid input. 2 key.
    */
   @Test
   public void testParseVMKeyInValidInputDuplicateKeys() {
      String v2 = "{\"timestamp\": 1446041354, \"value\": 0.2, \"tags\": servers.kafka02 {\"path\": \"servers.kafka02.cpu.cpu0.irq\", \"what\": \"cpu\", \"type\": \"cpu0.irq\", \"target_type\": \"gauge\", \"server\": \"kafka02\"}}";
      try {
         FunctionsLibHelper.parseVMKey(v2);
         Assert.assertTrue(false);
      } catch (Exception e) {
         e.printStackTrace();
         Assert.assertTrue(true);
      }
   }
   
   /**
    * InValid input. partial key.
    */
   @Test
   public void testParseVMKeyInValidInputPartialKey() {
      String v2 = "{\"timestamp\": 1446041354, \"value\": 0.2, \"tags\": {\"path\": \"serverskafka02.cpu.cpu0.irq\", \"what\": \"cpu\", \"type\": \"cpu0.irq\", \"target_type\": \"gauge\", \"server\": \"kafka02\"}}";
      try {
         FunctionsLibHelper.parseVMKey(v2);
         Assert.assertTrue(false);
      } catch (Exception e) {
         e.printStackTrace();
         Assert.assertTrue(true);
      }
   }
   
   /**
    * InValid input. partial key.
    */
   @Test
   public void testParseVMKeyInValidInputNull() {
      String v2 = null;
      try {
         FunctionsLibHelper.parseVMKey(v2);
         Assert.assertTrue(false);
      } catch (Exception e) {
         e.printStackTrace();
         Assert.assertTrue(true);
      }
   }
}
