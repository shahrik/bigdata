/**
 * UHA-2861 Event for VM performance issues:
 *    CPU
 *    MEM
 *    DiskUsage
 *    Network
 * Test cases for the system metric collectors
 * for the VM's.
 * @author kambiz.shahri
 *
 */
package com.amadeus.spark.uha2861;