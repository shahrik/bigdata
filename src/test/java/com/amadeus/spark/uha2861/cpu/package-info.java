/**
 * UHA-2861 VM Stats CPU test cases.
 * @author Kambiz.Shahri
 *
 */
package com.amadeus.spark.uha2861.cpu;