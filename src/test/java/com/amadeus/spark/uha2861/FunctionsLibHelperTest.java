package com.amadeus.spark.uha2861;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper class that has utilities that the functions lib. classes use.
 * @author Kambiz.Shahri
 *
 */
public class FunctionsLibHelperTest {
   
   public static void main(String[] args) {
      
      String input = "{\"timestamp\": 1446470351, \"value\": 57.90, \"tags\": {\"path\": \"servers.kafka04.cpu.total.idle\", \"what\": \"cpu\", \"type\": \"total.idle\", \"target_type\": \"gauge\", \"server\": \"kafka04\"}}"
            + "{\"timestamp\": 1446470352, \"value\": 65.30, \"tags\": {\"path\": \"servers.kafka04.cpu.total.idle\", \"what\": \"cpu\", \"type\": \"total.idle\", \"target_type\": \"gauge\", \"server\": \"kafka04\"}}"
            + "{\"timestamp\": 1446470352, \"value\": 2.60, \"tags\": {\"path\": \"servers.kafka04.cpu.total.idle\", \"what\": \"cpu\", \"type\": \"total.idle\", \"target_type\": \"gauge\", \"server\": \"kafka04\"}}"
            + "{\"timestamp\": 1446470352, \"value\": 98.97, \"tags\": {\"path\": \"servers.kafka04.cpu.total.idle\", \"what\": \"cpu\", \"type\": \"total.idle\", \"target_type\": \"gauge\", \"server\": \"kafka04\"}}"
            + "{\"timestamp\": 1446470353, \"value\": 65.12, \"tags\": {\"path\": \"servers.kafka04.cpu.total.idle\", \"what\": \"cpu\", \"type\": \"total.idle\", \"target_type\": \"gauge\", \"server\": \"kafka04\"}}";

      String doubleRegexPattern = "(\\d+\\.\\d+)+";
      List<String> cpuStreamValues = stripOutValues(input, doubleRegexPattern);
      List<Double> cpuActualValues = stringToDouble(cpuStreamValues);
      System.out.println("Number of CPU's over 95% for this server::["+calculateCPULoadCount(cpuActualValues, (double)95.0)+"]");
   }

   /**
    * Strips out the values based on the incoming pattern.
    * @param input the string that contains recurring numeric values as such:["value": NUMBER,]
    * @param inputNumberPatter
    * @return
    */
   public static List<String> stripOutValues(String input, String inputNumberPattern) {

      Pattern pattern = Pattern.compile(inputNumberPattern);
      Matcher matcher = pattern.matcher(input);
      ArrayList<String> listBuffer = new ArrayList<String>();
      while (matcher.find()) {
         listBuffer.add(matcher.group(0));
      }
      System.out.println(">>>" + listBuffer);

      return (listBuffer);
   }

   /**
    * Converts the value strings to double
    * 
    * @param input
    * @return
    */
   public static List<Double> stringToDouble(List<String> input) {
      assert input != null && !input.isEmpty();

      ArrayList<Double> listBuffer = new ArrayList<>();
      for (String string : input) {
         try {
            listBuffer.add(Double.valueOf(string));
         } catch (NumberFormatException e) {
            // TODO::LOGGER
            System.out.println("Parsing failed for a Double::[" + string + "]");
            e.printStackTrace();
         }
      }
      if (listBuffer.isEmpty()) {
         // TODO::LOGGER
         System.out
               .println("Output had no entries. Most probably input was NaN.");
      }
      return listBuffer;
   }
   
   /**
    * Counts the number of CPULoads that are above the threshold
    * @param input
    * @return
    */
   public static int calculateCPULoadCount(List<Double> input, double cpuLoadThreshold) {
      int i=0;
      for (Double double1 : input) {
         System.out.println("Calculated CPU Load was::["+((double)100.0 - double1)+"] cpuLoadThreshold::["+cpuLoadThreshold+"]");
         if(((double)100.0 - double1) > cpuLoadThreshold) {
            ++i;     
         }
      }
      return i;
   }
}
