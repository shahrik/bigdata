package com.amadeus.spark.uha2861;

import java.util.Properties;

import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

/**
 * Created by user on 8/4/14.
 */
public class KafkaProducerUtility {
   private static String targetTopic = "metrics.system";
   private static String targetHost = "dev-uha-06";

   private static String targetHostPort = "9092";

   public static void publishMessage(String inputMessage) {
      Properties properties = new Properties();
      properties.put("metadata.broker.list", "dev-uha-06:9092");
      properties.put("serializer.class", "kafka.serializer.StringEncoder");
      ProducerConfig producerConfig = new ProducerConfig(properties);
      kafka.javaapi.producer.Producer<String, String> producer = new kafka.javaapi.producer.Producer<String, String>(
            producerConfig);
      KeyedMessage<String, String> message = new KeyedMessage<String, String>(
            targetTopic, inputMessage);
      producer.send(message);
      producer.close();
   }

   /**
    * @return
    */
   public static String getTargetTopic() {
      return targetTopic;
   }

   /**
    * @param targetTopic
    */
   public static void setTargetTopic(String targetTopic) {
      KafkaProducerUtility.targetTopic = targetTopic;
   }

   /**
    * @return
    */
   public static String getTargetHost() {
      return targetHost;
   }

   /**
    * @param targetHost
    */
   public static void setTargetHost(String targetHost) {
      KafkaProducerUtility.targetHost = targetHost;
   }

   /**
    * @return
    */
   public static String getTargetHostPort() {
      return targetHostPort;
   }

   /**
    * @param targetHostPort
    */
   public static void setTargetHostPort(String targetHostPort) {
      KafkaProducerUtility.targetHostPort = targetHostPort;
   }
}