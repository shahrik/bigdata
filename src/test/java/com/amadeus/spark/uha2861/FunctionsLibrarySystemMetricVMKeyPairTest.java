package com.amadeus.spark.uha2861;

import org.junit.Assert;
import org.junit.Test;

import com.amadeus.spark.functions.FunctionsLibrary;

import scala.Tuple2;

public class FunctionsLibrarySystemMetricVMKeyPairTest {
   
   FunctionsLibrary.VMHasAPair vmHasAPair = new FunctionsLibrary.VMHasAPair();

   /**
    * Valid input. 1 key.
    * N.B. this test was before UHA-2902 when the VM stream data had no 
    * DataCenterId or ZoneId...therefore this test is for the 2nd part of
    * the composite key which would be the latter and the VM server details.
    */
   @Test
   public void testParseVMKeyValidInput() {
      String expectedValue = "servers.kafka02";
      String v2 = "{\"timestamp\": 1446041354, \"value\": 0.2, \"tags\": {\"path\": \"servers.kafka02.cpu.cpu0.irq\", \"what\": \"cpu\", \"type\": \"cpu0.irq\", \"target_type\": \"gauge\", \"server\": \"kafka02\"}}";
      Tuple2<String, String> outputTupe2=null;
      try {
         outputTupe2 = vmHasAPair.call(v2);
         Assert.assertEquals(expectedValue, outputTupe2._1());
      } catch (Exception e) {
         e.printStackTrace();
         Assert.assertTrue(false);
      }
   }
}
