package com.amadeus.spark.config;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author David Santa-Olalla
 *
 */
public class OnPropertyCondition implements Condition {
   private static final Logger log = LoggerFactory
         .getLogger(OnPropertyCondition.class);

   /* (non-Javadoc)
    * @see org.springframework.context.annotation.Condition#matches(org.springframework.context.annotation.ConditionContext, org.springframework.core.type.AnnotatedTypeMetadata)
    */
   @Override
   public boolean matches(ConditionContext context,
         AnnotatedTypeMetadata metadata) {
      boolean trueIfPropertyNotPresent = extractIgnoreWhenPropertyNotPresentFromAnnotation(metadata);
      String propertyKey = extractLabelFromAnnotation(metadata);

      if (!isLabelDefined(propertyKey)) {
         propertyKey = extractNameFromBean(metadata);
      }
      String matchingValue = extractMatchingValueFromAnnotation(metadata);

      Environment env = context.getEnvironment();

      if ((env.containsProperty(propertyKey) && env.getProperty(propertyKey)
            .equals(matchingValue))
            || (!env.containsProperty(propertyKey) && trueIfPropertyNotPresent)) {
         log.info("Enabling the instantiation of a bean refered by the label:"
               + propertyKey);
         return true;
      }

      log.info("Disabling the instantiation of a bean refered by the label:"
            + propertyKey);
      return false;
   }

   /**
    * @param metadata
    * @return
    */
   private String extractLabelFromAnnotation(AnnotatedTypeMetadata metadata) {
      Map<String, Object> attributes = metadata.getAnnotationAttributes(
            ConditionalOnProperty.class.getName(), true);
      return (String) attributes.get("label");
   }

   /**
    * @param metadata
    * @return
    */
   private String extractMatchingValueFromAnnotation(
         AnnotatedTypeMetadata metadata) {
      Map<String, Object> attributes = metadata.getAnnotationAttributes(
            ConditionalOnProperty.class.getName(), true);
      return (String) attributes.get("matchingValue");
   }

   /**
    * @param metadata
    * @return
    */
   private boolean extractIgnoreWhenPropertyNotPresentFromAnnotation(
         AnnotatedTypeMetadata metadata) {
      Map<String, Object> attributes = metadata.getAnnotationAttributes(
            ConditionalOnProperty.class.getName(), true);
      return (boolean) attributes.get("ignoreWhenPropertyNotPresent");
   }

   /**
    * @param metadata
    * @return
    */
   private String extractNameFromBean(AnnotatedTypeMetadata metadata) {
      String propertyKey;
      Map<String, Object> attributesBean = metadata.getAnnotationAttributes(
            Bean.class.getName(), true);
      propertyKey = ((String[]) attributesBean.get("name"))[0];
      return propertyKey;
   }

   /**
    * @param propertyKey
    * @return
    */
   private boolean isLabelDefined(String propertyKey) {
      return !propertyKey.equals("");
   }

}
