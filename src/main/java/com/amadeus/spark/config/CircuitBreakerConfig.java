package com.amadeus.spark.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.amadeus.spark.KafkaStreamProcessor;
import com.amadeus.spark.collectors.LqsCollector;
import com.amadeus.spark.collectors.LqsCollectorComposite;
import com.amadeus.spark.collectors.LqsCollectorMessage;
import com.amadeus.spark.collectors.MetricsCollector;
import com.amadeus.spark.collectors.MetricsCollectorAverageDuration;
import com.amadeus.spark.collectors.MetricsCollectorComposite;
import com.amadeus.spark.collectors.MetricsCollectorMessageTypePercentage;
import com.amadeus.spark.collectors.MetricsCollectorMessageTypeThroughput;
import com.amadeus.spark.collectors.MetricsCollectorResponseTimeBreachPercentage;
import com.amadeus.spark.collectors.MetricsCollectorThroughput;
import com.amadeus.spark.collectors.MetricsCollectorWindowedSLABreachCollector;
import com.amadeus.spark.collectors.SLACollector;
import com.amadeus.spark.collectors.SLACollectorComposite;
import com.amadeus.spark.detectors.MessageTypeAnomalyDetection;
import com.amadeus.spark.detectors.ResponsetimeAnomalyDetection;
import com.amadeus.spark.detectors.SlaBreachDetector;
import com.amadeus.spark.detectors.SlaBreachDetectorComposite;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;
import com.amadeus.spark.sla.NotificationService;
import com.amadeus.spark.sla.NotificationServiceCircuitBreaker;
import com.amadeus.spark.sla.NotificationServiceComposite;
import com.amadeus.spark.sla.NotificationServiceEventMetricPublisher;
import com.amadeus.spark.sla.NotificationServiceKafkaMetricPublisher;
import com.amadeus.spark.sla.model.MessageType;
import com.amadeus.spark.utils.SparkOperations;

/**
 * This is where the spark configuration points the publisher, the consumers 
 * the collectors to the Kafka installation.
 * @author James King, David Santa-Olalla
 *
 */
@Configuration
@ComponentScan(basePackages = "com.amadeus")
@PropertySources({
      @PropertySource({"classpath:circuitbreaker.properties", "blacklist-spring-beans.properties"}),
      @PropertySource(value = { "classpath:override-circuitbreaker.properties" }, ignoreResourceNotFound = true) })
public class CircuitBreakerConfig {

   private static final Logger log = LoggerFactory.getLogger(CircuitBreakerConfig.class);
   
   @Autowired @Qualifier("slaBreachMetricsNotifier")
   NotificationService slaBreachNotificationService; 
   
   @Autowired @Qualifier("eventsMetricsNotifier")
   NotificationService eventNotificationService; 
   
   @Autowired @Qualifier("applicationMetricsNotifier")
   NotificationService applicationNotificationService; 
   
   @Autowired @Qualifier("lqsNotifier")
   NotificationService lqsNotificationService; 
   /*
    * Notifiers
    */
   /**
    * This bean is a collection of the 2 notification services
    * that the MessageTypeAnomalyDetection and ReponsetimeAnomalyDetection
    * use. So here the so called kafkaNotificationServices specifies a broadcast
    * model for the same message. 
    * It specifies the 2 destinations, and then NotificationServiceComposite is 
    * _ACTUALLY_ the notification service that is returned to the caller.
    * Therein, the same notification is sent to the services in this method.
    * Confused, well...you might have been yes...but hopefully not now.
    * @return
    */
   @Bean(name="kafkaNotificationServices")
   public NotificationService notificationService() {
      List<NotificationService> notifications = new ArrayList<NotificationService>();
      notifications.add(slaBreachNotificationService);
      notifications.add(eventNotificationService);
      return new NotificationServiceComposite(notifications);
   }
   @Bean(name = "slaBreachMetricsNotifier")
   public NotificationServiceKafkaMetricPublisher notificationServiceKafkaSlaBreachMetricPublisher(
         KafkaProducer<String, String> kafkaProducer,
         @Value("${notifier.kafka.producer.topic}") String topic) {
      log.info("Creating a notification service publishing on [{}]", topic);
      return new NotificationServiceKafkaMetricPublisher(kafkaProducer, topic);
   }
   @Bean(name = "eventsMetricsNotifier")
   public NotificationServiceEventMetricPublisher notificationServiceEventMetricPublisher(
         KafkaProducer<String, String> kafkaProducer,
         @Value("${notifier.kafka.producer.events.topic}") String topic) {
      log.info("Creating a notification service publishing on [{}]", topic);
      return new NotificationServiceEventMetricPublisher(kafkaProducer, topic);
   }
   @Bean(name = "applicationMetricsNotifier")
   public NotificationServiceKafkaMetricPublisher applicationNotificationServiceKafkaMetricPublisher(
         KafkaProducer<String, String> kafkaProducer,
         @Value("${notifier.kafka.producer.topic.application.metrics}") String topic) {
      log.info("Creating a notification service publishing on [{}]", topic);
      return new NotificationServiceKafkaMetricPublisher(kafkaProducer, topic);
   }
   @Bean(name = "lqsNotifier")
   public NotificationServiceKafkaMetricPublisher lqsNotificationServiceLqsPublisher(
         KafkaProducer<String, String> kafkaProducer,
         @Value("${notifier.kafka.producer.topic.lqs.metrics}") String topic) {
      log.info("Creating a notification service publishing on [{}]", topic);
      return new NotificationServiceKafkaMetricPublisher(kafkaProducer, topic);
   }
   @Bean
   public NotificationServiceCircuitBreaker notificationServiceCircuitBreaker() {
      return new NotificationServiceCircuitBreaker();
   }
   
   /*
    * Detectors
    */
   @Bean(name = "messageTypeCtrlAnomalyDetectionCtrl")
   @ConditionalOnProperty
   public MessageTypeAnomalyDetection messageTypeAnomalyDetectionCtrl() {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE};
      return new MessageTypeAnomalyDetection(groupingFields, MessageType.MESSAGE_CTRL);
   }
   @Bean(name = "messageTypeRtoAnomalyDetectionRto")
   @ConditionalOnProperty
   public MessageTypeAnomalyDetection messageTypeAnomalyDetectionRto() {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE};
      return new MessageTypeAnomalyDetection(groupingFields, MessageType.MESSAGE_RTO);
   }
   @Bean(name = "anomalyDetection")
   public SlaBreachDetector<JavaPairDStream<String, String>> anomalyDetectionBean() {
      return new SlaBreachDetectorComposite();
   }
   @Bean(name= "responsetimeAnomalyDetection")
   @ConditionalOnProperty
   public ResponsetimeAnomalyDetection responsetimeAnomalyDetection(
         @Value("${spark.detector.responsetime.sla.threshold}") double slaResponseTimeThreshold,
         @Value("${spark.detector.responsetime.sla.threshold.breach.percent}") int slaResponseTimeThresholdPercent) {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE, LogEventKeyField.SERVICE_NAME};
      return new ResponsetimeAnomalyDetection(groupingFields, slaResponseTimeThreshold,
                                             slaResponseTimeThresholdPercent);
   }
   @Bean
   public KafkaStreamProcessor messageTypeAnomalyDetection(
         @Value("${kafka.consumer.topic}") String topic,
         @Value("${kafka.consumer.bootstrap.servers}") String kafkaConsumerBootstrapServers,
         @Value("${microbatch.duration}") int microbatchDuration) {
      log.info(
            "Creating a message anomaly detection listening on topic [{}] "
                  + "zookeeper [{}]  groupid [{}] with a microbatch duration of [{}] ",
            topic, kafkaConsumerBootstrapServers, microbatchDuration);
      return new KafkaStreamProcessor(topic, kafkaConsumerBootstrapServers,
            microbatchDuration);
   }

   /*
    * Collectors
    */
   @Bean(name = "metricsCollector")
   public MetricsCollector<JavaPairDStream<String, String>> metricsCollectorComposite() {
      return new MetricsCollectorComposite();
   }
   @Bean(name = "metricsCollectorAverageDuration")
   @ConditionalOnProperty
   public MetricsCollectorAverageDuration metricsCollectorAverageDuration() 
   {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE, LogEventKeyField.SERVICE_NAME};
      return new MetricsCollectorAverageDuration(groupingFields);
   }
   @Bean(name = "metricsCollectorResponseTimeBreachPercentage")
   @ConditionalOnProperty
   public MetricsCollectorResponseTimeBreachPercentage metricsCollectorBreachPercentage(
         @Value("${spark.detector.responsetime.sla.threshold}") double slaResponseTimeThreshold) {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE, LogEventKeyField.SERVICE_NAME};
      return new MetricsCollectorResponseTimeBreachPercentage( groupingFields, slaResponseTimeThreshold);
   }
   @Bean(name = "metricsCollectorMessageTypePercentageRto")
   @ConditionalOnProperty
   public MetricsCollectorMessageTypePercentage metricsCollectorMessageTypePercentageRto() {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE};
      return new MetricsCollectorMessageTypePercentage(groupingFields, MessageType.MESSAGE_RTO);
   }
   @Bean(name = "metricsCollectorThroughput")
   @ConditionalOnProperty
   public MetricsCollectorThroughput metricsCollectorThroughput() {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE, LogEventKeyField.SERVICE_NAME};
      return new MetricsCollectorThroughput(groupingFields);
   }

   @Bean(name = "metricsCollectorLqsThroughput")
   @ConditionalOnProperty
   public MetricsCollectorThroughput metricsCollectorLqsThroughput() {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE, LogEventKeyField.SERVICE_NAME};
      return new MetricsCollectorThroughput(groupingFields);
   }
   
   /**
    * This is a composite that contains all the bean names
    * that start with slaCollector.
    * This is a Spring "feature" that is hidden and might confuse.
    * @return
    */
   @Bean(name = "slaCollector")
   public SLACollector<JavaPairDStream<String, String>> slaCollectorComposite() {
      return new SLACollectorComposite();
   }
   
   @Bean(name = "lqsCollectors")
   public LqsCollector<JavaPairDStream<String, String>> lqsCollectorComposite() {
      return new LqsCollectorComposite();
   }
   @Bean(name = "lqsCollectorMessage")
   @ConditionalOnProperty
   public LqsCollectorMessage lqsCollectorMessage() 
   {
      return new LqsCollectorMessage();
   }

   /**
    * For the configuration of the meta collector.
    * It collects what we have already produced for RTO SLA breach
    * and generates an event based on the duration of the RTO SLA breaches
    * continuing.
    * N.B. if the increment is not an integer multiple of the Kafka consuming
    * interval, this collector will bomb out on startup.
    * @return
    */
   @Bean(name = "slaCollectorRTOSLABreach")
   @ConditionalOnProperty
   public MetricsCollectorWindowedSLABreachCollector metricsCollectorRTOSLABreach(
         @Value("${spark.collector.metricscollectorrtoslabreach.window.duration}") long windowDuration,
         @Value("${spark.collector.metricscollectorrtoslabreach.window.increment}") long windowIncrement,
         @Value("${spark.collector.metricscollectorrtoslabreach.filter.value}") String streamFilter,
         @Value("${microbatch.duration.in.millis}") long microbatchDuration,
         @Value("${spark.collector.metricscollectorrtoslabreach.metrictagtype.string.equiv}") String metricTagName,
         @Value("${spark.collector.metricscollectorrtoslabreach.regexnumberpatterntolookfor}") String calculationPattern,
         @Value("${spark.collector.metricscollectorrtoslabreach.threshholdvalue}") double threshholdValue,
         @Value("${spark.collector.metricscollectorrtoslabreach.rdd.processor.label}") String rddProcessorLabel,
         @Value("${spark.collector.metricscollectorrtoslabreach.pairer.label}") String pairerLabel) {
      return new MetricsCollectorWindowedSLABreachCollector(windowDuration,
            windowIncrement, streamFilter, microbatchDuration, metricTagName,
            calculationPattern, threshholdValue, rddProcessorLabel,
            pairerLabel);
   }
   /**
    * For the configuration of the meta collector.
    * It collects what we have already produced for CTRL SLA breach
    * and generates an event based on the duration of the RTO SLA breaches
    * continuing.
    * N.B. if the increment is not an integer multiple of the Kafka consuming
    * interval, this collector will bomb out on startup.
    * @return
    * @throws Exception 
    */
   @Bean(name = "slaCollectorCTRLSLABreach")
   @ConditionalOnProperty
   public MetricsCollectorWindowedSLABreachCollector metricsCollectorCTRLSLABreach(
         @Value("${spark.collector.metricscollectorctrlslabreach.window.duration}") long windowDuration,
         @Value("${spark.collector.metricscollectorctrlslabreach.window.increment}") long windowIncrement,
         @Value("${spark.collector.metricscollectorctrlslabreach.filter.value}") String streamFilter,
         @Value("${microbatch.duration.in.millis}") long microbatchDuration,
         @Value("${spark.collector.metricscollectorctrlslabreach.metrictagtype.string.equiv}") String metricTagName,
         @Value("${spark.collector.metricscollectorctrlslabreach.regexnumberpatterntolookfor}") String calculationPattern,
         @Value("${spark.collector.metricscollectorctrlslabreach.threshholdvalue}") double threshholdValue,
         @Value("${spark.collector.metricscollectorctrlslabreach.rdd.processor.label}") String rddProcessorLabel,
         @Value("${spark.collector.metricscollectorctrlslabreach.pairer.label}") String pairerLabel) {
      return new MetricsCollectorWindowedSLABreachCollector(windowDuration,
            windowIncrement, streamFilter, microbatchDuration, metricTagName,
            calculationPattern, threshholdValue, rddProcessorLabel,
            pairerLabel);
   }
   /**
    * This is a composite that contains all the bean names
    * that start with systemMetricsCollector.
    * This is a Spring "feature" that is hidden and might confuse.
    * The following are VM Collector instances based on their cateogry.
    * They all reuse the base type SystemMetricsVMWindowedCollector
    * @return
    */
   @Bean(name = "systemMetricsCollectorCPU")
   @ConditionalOnProperty
   public MetricsCollectorWindowedSLABreachCollector systemMetricsCPUCollector( 
         @Value("${spark.collector.vm.cpu.stream.filter.value}") String streamFilter,
         @Value("${microbatch.duration.in.millis}") long microbatchDuration,
         @Value("${spark.collector.vm.cpu.regexnumberpatterntolookfor}") String calculationPattern,
         @Value("${spark.collector.vm.cpu.threshholdvalue}") double threshholdValue,
         @Value("${spark.collector.vm.cpu.window.duration}") long windowDuration,
         @Value("${spark.collector.vm.cpu.window.increment}") long windowIncrement,
         @Value("${spark.collector.vm.cpu.window.increment.metrictagtype.string.equiv}") String metricTagName, 
         @Value("${spark.collector.vm.cpu.rdd.processor.label}") String rddProcessorLabel,
         @Value("${spark.collector.vm.cpu.pairer.label}") String pairerLabel){
      return new MetricsCollectorWindowedSLABreachCollector(windowDuration,
            windowIncrement, streamFilter, microbatchDuration, metricTagName,
            calculationPattern, threshholdValue, rddProcessorLabel,
            pairerLabel);
   }
   @Bean(name = "systemMetricsCollectorDiskSpace")
   @ConditionalOnProperty
   public MetricsCollectorWindowedSLABreachCollector systemMetricsDiskSpaceCollector( 
         @Value("${spark.collector.vm.disk.stream.filter.value}") String streamFilter,
         @Value("${microbatch.duration.in.millis}") long microbatchDuration,
         @Value("${spark.collector.vm.disk.regexnumberpatterntolookfor}") String calculationPattern,
         @Value("${spark.collector.vm.disk.threshholdvalue}") double threshholdValue,
         @Value("${spark.collector.vm.disk.window.duration}") long windowDuration,
         @Value("${spark.collector.vm.disk.window.increment}") long windowIncrement,
         @Value("${spark.collector.vm.disk.window.increment.metrictagtype.string.equiv}") String metricTagName, 
         @Value("${spark.collector.vm.disk.rdd.processor.label}") String rddProcessorLabel,
         @Value("${spark.collector.vm.disk.pairer.label}") String pairerLabel){
      return new MetricsCollectorWindowedSLABreachCollector(windowDuration,
            windowIncrement, streamFilter, microbatchDuration, metricTagName,
            calculationPattern, threshholdValue, rddProcessorLabel,
            pairerLabel);
   }
   @Bean(name = "systemMetricsCollectorMemory")
   @ConditionalOnProperty
   public MetricsCollectorWindowedSLABreachCollector systemMetricsMemoryCollector( 
         @Value("${spark.collector.vm.mem.stream.filter.value}") String streamFilter,
         @Value("${microbatch.duration.in.millis}") long microbatchDuration,
         @Value("${spark.collector.vm.mem.regexnumberpatterntolookfor}") String calculationPattern,
         @Value("${spark.collector.vm.mem.threshholdvalue}") double threshholdValue,
         @Value("${spark.collector.vm.mem.window.duration}") long windowDuration,
         @Value("${spark.collector.vm.mem.window.increment}") long windowIncrement,
         @Value("${spark.collector.vm.mem.window.increment.metrictagtype.string.equiv}") String metricTagName, 
         @Value("${spark.collector.vm.disk.rdd.processor.label}") String rddProcessorLabel,
         @Value("${spark.collector.vm.disk.pairer.label}") String pairerLabel){
      return new MetricsCollectorWindowedSLABreachCollector(windowDuration,
            windowIncrement, streamFilter, microbatchDuration, metricTagName,
            calculationPattern, threshholdValue, rddProcessorLabel,
            pairerLabel);
   }
   @Bean(name = "systemMetricsCollectorNetwork")
   @ConditionalOnProperty
   public MetricsCollectorWindowedSLABreachCollector systemMetricsNetworkCollector( 
         @Value("${spark.collector.vm.network.stream.filter.value}") String streamFilter,
         @Value("${microbatch.duration.in.millis}") long microbatchDuration,
         @Value("${spark.collector.vm.network.regexnumberpatterntolookfor}") String calculationPattern,
         @Value("${spark.collector.vm.network.threshholdvalue}") double threshholdValue,
         @Value("${spark.collector.vm.network.window.duration}") long windowDuration,
         @Value("${spark.collector.vm.network.window.increment}") long windowIncrement,
         @Value("${spark.collector.vm.network.window.increment.metrictagtype.string.equiv}") String metricTagName, 
         @Value("${spark.collector.vm.disk.rdd.processor.label}") String rddProcessorLabel,
         @Value("${spark.collector.vm.disk.pairer.label}") String pairerLabel){
      return new MetricsCollectorWindowedSLABreachCollector(windowDuration,
            windowIncrement, streamFilter, microbatchDuration, metricTagName,
            calculationPattern, threshholdValue, rddProcessorLabel,
            pairerLabel);
   }
   
   @Bean(name = "metricsCollectorMessageTypePercentageCTRL")
   @ConditionalOnProperty
   public MetricsCollectorMessageTypePercentage metricsCollectorMessageTypePercentageCTRL() {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE};
      return new MetricsCollectorMessageTypePercentage(groupingFields, MessageType.MESSAGE_CTRL);
   }
   @Bean(name = "metricsCollectorCTRLThroughput")
   @ConditionalOnProperty
   public MetricsCollectorMessageTypeThroughput metricsCollectorCTRLThroughput() {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE};
      return new MetricsCollectorMessageTypeThroughput(groupingFields, MessageType.MESSAGE_CTRL);
   }
   @Bean(name = "metricsCollectorRTOThroughput")
   @ConditionalOnProperty
   public MetricsCollectorMessageTypeThroughput metricsCollectorRTOThroughput() {
      LogEventKeyField[] groupingFields = {LogEventKeyField.DATACENTER, LogEventKeyField.ISOLATION_ZONE};
      return new MetricsCollectorMessageTypeThroughput(groupingFields, MessageType.MESSAGE_RTO);
   }
   
   
   /*
    *  Other Beans
    */

   @Bean
   public KafkaProducer<String, String> kafkaProducer(
         @Value("${notifier.kafka.producer.zookeeper}") String zookeeperHostPort,
         @Value("${notifier.kafka.producer.bootstrapservers}") String bootstrapServerHostPort) {
      Properties p = new Properties();
      p.put("value.serializer",
            "org.apache.kafka.common.serialization.StringSerializer");
      p.put("key.serializer",
            "org.apache.kafka.common.serialization.StringSerializer");
      p.put("zk.connect", zookeeperHostPort);
      p.put("bootstrap.servers", bootstrapServerHostPort);
      log.info(
            "Creating a kafka producer for notification service listening on zookeeper [{}], masterHost [{}]",
            zookeeperHostPort, bootstrapServerHostPort);
      return new KafkaProducer<String, String>(p);
   }
   
   @Bean
   public SparkOperations sparkOperations() {
      return new SparkOperations();
   }
   
   @Bean
   public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
      return new PropertySourcesPlaceholderConfigurer();
   }
}