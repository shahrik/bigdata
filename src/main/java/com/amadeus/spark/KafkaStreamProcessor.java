package com.amadeus.spark;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.amadeus.spark.collectors.LqsCollector;
import com.amadeus.spark.collectors.MetricsCollector;
import com.amadeus.spark.collectors.SLACollector;
import com.amadeus.spark.detectors.SlaBreachDetector;
import com.amadeus.spark.exception.CollectorTechnicalException;
import com.esotericsoftware.minlog.Log;

import kafka.serializer.StringDecoder;

/**
 * This is the 2nd step of the application.
 * Where the streaming starts from the Kafka 
 * @author James King, David Santa-Olalla, Kambiz Shahri
 *
 */
public class KafkaStreamProcessor {
	
	private String topic;
	private String kafkaConsumerBootstrapServers;
	private int duration;
	
	@Autowired
	@Qualifier("anomalyDetection")
	private SlaBreachDetector<JavaPairDStream<String, String>> detector;
	
	@Autowired
	@Qualifier("metricsCollector")
	private MetricsCollector <JavaPairDStream<String, String>> metricsCollector;
	
	@Autowired
   @Qualifier("slaCollector")
	private SLACollector <JavaPairDStream<String, String>> slaCollectors;
	
	@Autowired
	@Qualifier("lqsCollectors")
	private LqsCollector <JavaPairDStream<String, String>> lqsCollectors;
	
	private static final Logger log = LoggerFactory
			.getLogger(KafkaStreamProcessor.class);
	
	/**
	 * @param topic
	 * @param kafkaConsumerBootstrapServers
	 * @param duration
	 */
	public KafkaStreamProcessor(String topic, String kafkaConsumerBootstrapServers, int duration) {
		this.topic = topic;
		this.kafkaConsumerBootstrapServers = kafkaConsumerBootstrapServers;
		this.duration = duration;
	}

	/**
	 * Processes the streams.
	 */
	public void process() {
		
		JavaStreamingContext spark = createSparkContext(duration);
		
		JavaPairDStream<String, String> input = null;
		JavaPairDStream<String, String> eventsInput = null;
      JavaPairDStream<String, String> systemMetricsInput = null;
		JavaPairDStream<String, String> lqsMetricsInput = null;
		
		
		input = retrieveBatchInput(spark, topic, kafkaConsumerBootstrapServers);
		eventsInput = retrieveBatchInput(spark, "events.circuitbreaker", kafkaConsumerBootstrapServers);
		systemMetricsInput = retrieveBatchInput(spark, "metrics.system", kafkaConsumerBootstrapServers);
		log.debug("Just retrieved as batch input eventsInput:["+eventsInput+"]");
		log.debug("Just retrieved as batch input systemMetricsInput:["+systemMetricsInput+"]");
	   lqsMetricsInput = retrieveBatchInput(spark, "metrics.queues", kafkaConsumerBootstrapServers);
               
		detector.detect(input);
		metricsCollector.collect(input);
		try {
         slaCollectors.collect(eventsInput, systemMetricsInput);
      } catch (CollectorTechnicalException e) {
         Log.error("Failed to initiate slaCollectors."+e);
      }
		lqsCollectors.collect(lqsMetricsInput);

		spark.start();
		spark.awaitTermination();
	}

	/**
	 * @param spark
	 * @param topics
	 * @param kafkaConsumerBootstrapServers
	 * @return
	 */
	private JavaPairDStream<String, String> retrieveBatchInput(JavaStreamingContext spark, String topics, String kafkaConsumerBootstrapServers) {
		
		HashSet<String> topicsSet = new HashSet<String>(Arrays.asList(topics.split(",")));
		HashMap<String, String> kafkaParams = new HashMap<String, String>();
		kafkaParams.put("bootstrap.servers", kafkaConsumerBootstrapServers);
		
		JavaPairDStream<String, String> input = KafkaUtils.createDirectStream(
				spark, 
				String.class,
		        String.class,
		        StringDecoder.class,
		        StringDecoder.class,
		        kafkaParams,
		        topicsSet
		);
		
		if(log.isDebugEnabled()) {
			input.print();
		}
		
		return input;
	}
	
	/**
	 * @param duration
	 * @return
	 */
	private JavaStreamingContext createSparkContext(int duration) {
		SparkConf sparkConf = new SparkConf();
		log.info("SPARK CONF:{}" , sparkConf.toDebugString());
		
		JavaStreamingContext ssc = new JavaStreamingContext(sparkConf,
											Durations.seconds(duration));
		return ssc;
	}
}