/**
 * Contains the Spark functions leveraged
 * for doing the work for the generic collector framework.
 * @author Kambiz.Shahri
 */
package com.amadeus.spark.functions;