package com.amadeus.spark.functions;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagSla;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagType;

import scala.Tuple2;

/**
 * This is a reusable functions library (wrapper around static functions) that was 
 * started for the purposes of UHA-2465 Event for RTOs. 
 * It is reused across UHA-2465 and 2466 and 1865.
 * In essence it is a collection of static classes that are used
 * as functions for transforming (calculating) Spark stream RDD's.
 * It is now a quasi-generic functions library reused across multiple collector types.
 * @author Kambiz.Shahri
 *
 */
public class FunctionsLibrary {
   private static final Logger LOG = LoggerFactory
         .getLogger(FunctionsLibrary.class);
   
   private static ESMNotifierProxy eSMRTOEventNotifier = new ESMNotifierProxy();
   private static long microbatchDurationInMilliSeconds;
   private static String eventDescriptionTemplate = "Event for %s::Continuous SLA Breaches for::[%s] seconds or more.";
   
   /**
    * Returns a rddProcessor based on the label provided which
    * maps to the type contained in this wrapper of static classes.
    * I originally wanted the rddProcessorLabel to == camelCase type name, 
    * but, I decided to be conformant with the properties file.
    * @param rddProcessorLabel
    * @return
    */
   public static Function <JavaPairRDD<String, Iterable<String>>, Void> rddProcessorFactory(
         String rddProcessorLabel,
         String calculationPattern,
         Double threshHoldValue,
         Long windowDuration,
         Long windowIncrement,
         MetricTagType metricType) {
      switch (rddProcessorLabel) {
      case "slabreachrddcalculator":
         FunctionsLibrary.SLABreachRDDCalculator sLABreachRDDCalculator = new FunctionsLibrary.SLABreachRDDCalculator (
               calculationPattern,
               threshHoldValue,
               windowDuration,
               windowIncrement, 
               metricType);
         LOG.debug("Returning FunctionsLibrary.SLABreachRDDCalculator()::["+sLABreachRDDCalculator+"]");
         return sLABreachRDDCalculator;
      case "systemmetricrddcalculator":
         FunctionsLibrary.SystemMetricRDDCalculator systemMetricRDDCalculator = new FunctionsLibrary.SystemMetricRDDCalculator (
               calculationPattern,
               threshHoldValue,
               windowDuration,
               windowIncrement, 
               metricType);
         LOG.debug("Returning FunctionsLibrary.SystemmetricRDDcCalculator()::["+systemMetricRDDCalculator+"]");
         return systemMetricRDDCalculator;
      default:
         LOG.error("Unexpected calculatorLabel! Unable to instantiate calculator for label::["+rddProcessorLabel+"]");
         break;
      }
      return null;
   }
   
   /**
    * @return
    */
   public static PairFunction<String, String, String> pairerFactory(
         String pairerLabel) {

      switch (pairerLabel) {
      case "ihaveapair":
         FunctionsLibrary.IHaveAPair iHaveAPair = new FunctionsLibrary.IHaveAPair();
         LOG.debug("Returning FunctionsLibrary.IHaveAPair()::["+iHaveAPair+"]");
         return iHaveAPair;
      case "vmhasapair":
         FunctionsLibrary.VMHasAPair vMHasAPair = new FunctionsLibrary.VMHasAPair();
         LOG.debug("Returning FunctionsLibrary.VMHasAPair()::["+vMHasAPair+"]");
         return vMHasAPair;
      default:
         LOG.error("Unexpected pairerLabel! Unable to instantiate calculator for label::["+pairerLabel+"]");
         break;
      }
      return null;
   }
   
   /**
    * Function to ensure the SLA breach duration was => the threshold for the event.
    * @author kambiz.shahri
    *
    */
   public static class SLABreachRDDCalculator implements Function <JavaPairRDD<String, Iterable<String>>, Void> {
      /**
       * 
       */
      private static final long serialVersionUID = -7907581992725732178L;
      
      String regexNumberPatternToLookFor;
      Double threshHoldValue;
      Long windowDuration;
      Long windowIncrement;
      MetricTagType metricType;
      
      /**
       * Constructor.
       * @param callerNumberPattern
       */
      public SLABreachRDDCalculator(
            String callerNumberPattern,
            Double threshHoldValue,
            Long windowDuration,
            Long windowIncrement, 
            MetricTagType metricType) {
         this.regexNumberPatternToLookFor = callerNumberPattern;
         this.threshHoldValue = threshHoldValue;
         this.windowDuration = windowDuration;
         this.windowIncrement = windowIncrement;
         this.metricType = metricType;
      }
      
      /* (non-Javadoc)
       * @see org.apache.spark.api.java.function.Function#call(java.lang.Object)
       */
      @Override
      public Void call(JavaPairRDD<String, Iterable<String>> v1)
            throws Exception {
         
         Map<String, Iterable<String>> v1Map = v1.collectAsMap();
         long breachCount = 0;
         LOG.trace(metricType+"::Map Key Count::["+v1Map.keySet().size()+"]");
         for (Map.Entry<String, Iterable<String>> entry : v1Map.entrySet()) {
            LOG.trace(metricType+"::Map Key::["+entry.getKey().toString()+"]");
            breachCount = FunctionsLibHelper.countPatternOccurrences(entry.getValue().toString(), regexNumberPatternToLookFor);
            LOG.debug(metricType+"::SLABreach Count was::["+breachCount+"]");          
            long breachThreshold = windowDuration/getMicrobatchDurationInMilliSeconds();
            LOG.debug(metricType+"::breachThreshold Count was::["+breachThreshold+"]"); 
            if(breachCount >= breachThreshold) {
                LOG.debug(metricType+"::>>>>>>>>>>>KO EVENT");
                //Get all data items needed for the transmisstion to ESM
                String[] keyParts = FunctionsLibHelper.stripOutDatacenterEtAl(entry.getKey().toString());
                String formattedDescription = null;
                AnalysisMetric.MetricTagType metricType = null;
                if(entry.getKey().contains(AnalysisMetric.MetricTagType.RTO.toString())) {
                   formattedDescription = String.format(eventDescriptionTemplate, AnalysisMetric.MetricTagType.RTO.toString(), windowDuration/1000); 
                   metricType = AnalysisMetric.MetricTagType.RTO;
                } else if (entry.getKey().contains(AnalysisMetric.MetricTagType.CTRL.toString())) {
                   formattedDescription = String.format(eventDescriptionTemplate, AnalysisMetric.MetricTagType.CTRL.toString(), windowDuration/1000); 
                   metricType = AnalysisMetric.MetricTagType.CTRL;
                } else {
                   LOG.error(metricType+"::Event key did not contain either CTRL or RTO");
                }
                //Do it
                eSMRTOEventNotifier.transmit(
                      Instant.now().getEpochSecond(), 
                      5,//TODO::current RTO SLA breach value, needs to be parameterized from this: spark.detector.responsetime.sla.threshold.breach.percent=5
                      AnalysisMetric.MetricTagWhat.METRIC_PERCENTAGE, 
                      metricType, 
                      MetricTagSla.SLA_KO, 
                      (keyParts == null || keyParts.length < 2) ? "datacenterId?":keyParts[1],
                      (keyParts == null || keyParts.length < 4) ? "zoneId?":keyParts[3], 
                      "siloId?", 
                      "server?", 
                      (keyParts == null || keyParts.length < 6) ? "serviceName?":keyParts[5], 
                      formattedDescription);
            } else {
               LOG.debug(metricType+"::>>>>>>>>>>>NO---> EVENT");
            }
         }
         return null;
      }
   }
   
   /**
    * This is now a generic filter function that can be used for 
    * any collector. 
    * The constructor receives a filter for which it will match the
    * stream tuple. It either matches or it does not.
    * @author Kambiz.Shahri
    *
    */
   public static class SystemMetricsStreamFilter
         implements Function<Tuple2<String, String>, Boolean> {
      
      /**
      * 
      */
      private static final long serialVersionUID = 6267286352543596442L;

      private String filterValue;

      public SystemMetricsStreamFilter(String filterValue) {
         LOG.debug("filterValue::["+filterValue+"]");
         this.filterValue = filterValue;
      }

      @Override
      public Boolean call(Tuple2<String, String> keyValue) throws Exception {
         LOG.trace("Key::[" + keyValue._1() + "]");
         LOG.trace("Value::[" + keyValue._2() + "]");
         LOG.debug("Filtering Stream on::[" + filterValue + "]. ["
               + (keyValue._2().contains(filterValue) ? "Matched]"
                     : "No Match" + "]...Value of Stream::[" + filterValue
                           + "]"));
         return (keyValue._2().toLowerCase().contains(filterValue.toLowerCase()));
      }
   }
   
   /**
    * Assumption here is that there is no key.
    * See trace value.
    * @author Kambiz.Shahri
    *
    */
   public static class LinesFunction implements Function <Tuple2<String, String>, String> {

      /**
       * 
       */
      private static final long serialVersionUID = 1976480524314866805L;

      @Override
      public String call(Tuple2<String, String> v1) throws Exception {
         LOG.trace("Stream Key Value::["+v1._1+"]");
         return v1._2();
      }  
   }

   /**
    * Pair the DStream with a key.
    * 
    * @author Kambiz.Shahri
    *
    */
   public static class IHaveAPair
         implements PairFunction<String, String, String> {
      /**
       * 
       */
      private static final long serialVersionUID = 8736291743283226665L;

      @Override
      public Tuple2<String, String> call(String v1) throws Exception {
         LOG.trace("IHaveAPair::call()::input String was::==>[" + v1 + "]");
         String primaryKey = null;
         try {
            primaryKey = FunctionsLibHelper.parseOutKey(v1);
         } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Parsing datacenterId, zoneId, servicType failed.");
            primaryKey = "datacenterId-NOTFOUND-zoneId-NOTFOUND-serviceName-NOTFOUND";
         }
         LOG.trace("Now Keyed::["+primaryKey+"]...Value::["+v1+"]");
         return new Tuple2<String, String>(primaryKey, v1);
      }
   }
   
   /**
    * Pair the DStream with a composite key 
    * for VMs.
    * 
    * @author Kambiz.Shahri
    *
    */
   public static class VMHasAPair
         implements PairFunction<String, String, String> {
      /**
       * 
       */
      private static final long serialVersionUID = -1675456545590229290L;

      @Override
      public Tuple2<String, String> call(String v1) throws Exception {
         //TODO-when issue UHA-2902 is resolve-currently no datacentreId or
         //ZoneId is collected.
         String vmKey = null;
         //2ndary key-VM server details
         try {
            vmKey = FunctionsLibHelper.parseVMKey(v1);
         } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Parsing VM key failed.");
            vmKey = "server-NOTFOUND";
         }
         LOG.trace("Now VMKeyed::["+vmKey+"]...Value::["+v1+"]");
         LOG.trace("VM Composite key was:["+vmKey+"]");
         return new Tuple2<String, String>(vmKey, v1);
      }
   }

   
   
   
   /**
    * So here is the thing, this function flattens the RDD with one key.
    * So, if we filtered and keyed on the VMKey (serverid), this function will
    * on input provide the RDD's as a key value pair for that window of time.
    * Which means that if we filter the Iterable for all the matching cases 
    * we can then decide whether or not to transmit an event, satisfying the
    * case. So for instance, for the CPU, we find all the cpu.totla.idle's 
    * we calculate all the busy CPU which is 100-cpu.total.idle, if that count
    * is the frequency we excpet, we fire an event.
    * @author kambiz.shahri
    *
    */
   public static class SystemMetricRDDCalculator implements Function <JavaPairRDD<String, Iterable<String>>, Void> {
      /**
       * 
       */
      private static final long serialVersionUID = 3355216070518323567L;
      String regexNumberPatternToLookFor;
      Double threshHoldValue;
      Long windowDuration;
      Long windowIncrement;
      MetricTagType metricType;
      
      /**
       * Constructor.
       * @param callerNumberPattern
       */
      public SystemMetricRDDCalculator(
            String callerNumberPattern,
            Double threshHoldValue,
            Long windowDuration,
            Long windowIncrement, 
            MetricTagType metricType) {
         this.regexNumberPatternToLookFor = callerNumberPattern;
         this.threshHoldValue = threshHoldValue;
         this.windowDuration = windowDuration;
         this.windowIncrement = windowIncrement;
         this.metricType = metricType;
         LOG.debug("regexNumberPatternToLookFor::["+this.regexNumberPatternToLookFor+"] "+
         "threshHoldValue::["+this.threshHoldValue+"] "+
         "windowDuration::["+this.windowDuration+"] "+
         "windowIncrement::["+this.windowIncrement+"] "+
         "metricType::["+this.metricType+"]");
      }
      /* (non-Javadoc)
       * @see org.apache.spark.api.java.function.Function#call(java.lang.Object)
       */
      @Override
      public Void call(JavaPairRDD<String, Iterable<String>> v1)
            throws Exception {
         LOG.trace(metricType+"::SystemMetricRDDCalculator call for metricType::["+metricType+"]");
         //So we look through all the servers, parse the flattened RDD for the calculations
         Map<String, Iterable<String>> v1Map = v1.collectAsMap();
         for (Map.Entry<String, Iterable<String>> entry : v1Map.entrySet()) {
            List<String> streamValues = FunctionsLibHelper.stripOutValues(entry.getValue().toString(), regexNumberPatternToLookFor);
            List<Double> actualValues = FunctionsLibHelper.stringToDouble(streamValues);
            int breakingthresholdCount = FunctionsLibHelper.calculateLoadFromFreeList(actualValues, threshHoldValue);
            LOG.debug(metricType+"::Metrics >= threshold::["+threshHoldValue+"] was::["+breakingthresholdCount+"] for server::["+entry.getKey().toString()+"]");    
            if(FunctionsLibHelper.frequencyOfBreachAboveThreshold(windowDuration, breakingthresholdCount, getMicrobatchDurationInMilliSeconds(), metricType)) {
               LOG.debug(metricType+"::>>>>>>>>>>>KO EVENT");
               eSMRTOEventNotifier.transmit(Instant.now().getEpochSecond(), threshHoldValue,
                     AnalysisMetric.MetricTagWhat.METRIC_PERCENTAGE, metricType, MetricTagSla.SLA_KO, "datacenterId",
                     "zoneId", "siloId", entry.getKey().toString(), "serviceName", metricType+" was over threshold value:["+threshHoldValue+"] for ["+windowDuration/1000+"] seconds continually.");
            } else {
               LOG.debug(metricType+"::>>>>>>>>>>>NO---> EVENT");
            }
         }
         return null;
      }
   }
   
   /**
    * @return
    */
   public static long getMicrobatchDurationInMilliSeconds() {
      return microbatchDurationInMilliSeconds;
   }

   /**
    * @param microbatchDurationInMilliSeconds
    */
   public static void setMicrobatchDurationInMilliSeconds(
         long microbatchDurationInMilliSeconds) {
      FunctionsLibrary.microbatchDurationInMilliSeconds = microbatchDurationInMilliSeconds;
   }
}
