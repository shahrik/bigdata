package com.amadeus.spark.functions;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.amadeus.spark.config.CircuitBreakerConfig;
import com.amadeus.spark.sla.NotificationServiceEventMetricPublisher;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagSla;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagType;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagWhat;

/**
 * Event publisher proxy for ESM. Since we cannot inject using Spring into a
 * static instance variable, of a class that is never instantiated (the
 * functions library) we need this proxy.
 * 
 * @author Kambiz.Shahri
 */
public class ESMNotifierProxy {
   private static final Logger LOG = LoggerFactory
         .getLogger(ESMNotifierProxy.class);

   NotificationServiceEventMetricPublisher notificationServiceEventMetricPublisher;

   /**
    * Event transmission to EMDB
    */
   public void transmit(long timestamp, double value,
         MetricTagWhat metricTagWhat, MetricTagType metricTagType,
         MetricTagSla metricTagSla, String datacenterId, String zoneId,
         String siloId, 
         String server, 
         String serviceName, String description) {

      @SuppressWarnings("resource")
      ApplicationContext ctx = new AnnotationConfigApplicationContext(
            CircuitBreakerConfig.class);

      LOG.info(Arrays.deepToString(ctx.getBeanDefinitionNames()));
      LOG.info(Arrays.deepToString(ctx.getBeanDefinitionNames())
            .contains("eventsMetricsNotifier") ? "FOUND eventsMetricsNotifier"
                  : "NOT FOUND!!! eventsMetricsNotifier");

      notificationServiceEventMetricPublisher = (NotificationServiceEventMetricPublisher) ctx
            .getBean("eventsMetricsNotifier");
      try {
         notificationServiceEventMetricPublisher
               .publishMetric(new AnalysisMetric(
                                 timestamp, 
                                 value,
                                 metricTagWhat, 
                                 metricTagType, 
                                 metricTagSla, 
                                 datacenterId,
                                 zoneId, 
                                 siloId, 
                                 server, 
                                 serviceName, 
                                 description));
      } catch (Exception e) {
         LOG.error("Event for RTO or CTRL notification failed."+e.getStackTrace().toString()+"");
      }
   }
}
