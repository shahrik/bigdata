package com.amadeus.spark.functions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagType;

/**
 * Helper class of the functionsLibrary.
 * @author Kambiz.Shahri
 *
 */
public class FunctionsLibHelper {
   private static final Logger LOG = LoggerFactory
         .getLogger(FunctionsLibHelper.class);
   
   /**
    * SonarQube, you rule.
    */
   private FunctionsLibHelper() {
      
   }
    
   /**
    * Strips out the values based on the incoming pattern.
    * @param input the string that contains recurring numeric values as such:["value": NUMBER,]
    * @param inputNumberPatter string we will parse.
    * @return list of what we looked for with the pattern.
    */
   public static List<String> stripOutValues(String input, String inputNumberPattern) {

      Pattern pattern = Pattern.compile(inputNumberPattern);
      Matcher matcher = pattern.matcher(input);
      ArrayList<String> listBuffer = new ArrayList<>();
      while (matcher.find()) {
         listBuffer.add(matcher.group(0));
      }
      LOG.trace("Striped values from tuple v2::" + listBuffer);

      return listBuffer;
   }
   
   /**
    * Counts the occurrences of the pattern in the input
    * @param input the string that contains recurring pattern.
    * @param patternToUse the pattern to look for.
    * @return return the number of occurrences of the pattern.
    */
   public static long countPatternOccurrences(String input, String patternToUse) {

      Pattern pattern = Pattern.compile(patternToUse);
      Matcher matcher = pattern.matcher(input);
      ArrayList<String> listBuffer = new ArrayList<>();
      while (matcher.find()) {
         listBuffer.add(matcher.group(0));
      }
      LOG.trace("Striped values from tuple v2 before returning count::" + listBuffer);

      return (long)listBuffer.size();
   }

   /**
    * Converts the value strings to double.
    * 
    * @param input list of strings that should be floats.
    * @return a list of doubles equivalent to the input.
    */
   public static List<Double> stringToDouble(List<String> input) {
      assert input != null && !input.isEmpty();

      ArrayList<Double> listBuffer = new ArrayList<>();
      for (String string : input) {
         try {
            listBuffer.add(Double.valueOf(string));
         } catch (NumberFormatException e) {
            LOG.error("Parsing failed for a Double::[" + string + "]."+e);
         }
      }
      if (listBuffer.isEmpty()) {
         LOG.error("Output had no entries. Most probably input was NaN.");
      }
      return listBuffer;
   }
   
   /**
    * Counts the number of CPULoads that are above the threshold.
    * This is a reverse calculation, where the incoming is the negative of
    * what we actually want.
    * @param freeResourceList the list of FREE measurement of the resource. 
    * @return 100-the free resource list, which is the equivalent of what is taken up.
    */
   public static int calculateLoadFromFreeList(List<Double> freeResourceList, Double loadThreshold) {
      int i=0;
      for (Double free : freeResourceList) {
         LOG.debug("Calculated Load was::["+((double)100.0 - free)+"] cpuLoadThreshold::["+loadThreshold+"]");
         if(((double)100.0 - free) > loadThreshold) {
            ++i;     
         }
      }
      return i;
   }
   
   /**
    * Takes a window duration, takes a number of occurrences, takes the increment, 
    * and tries to figure out if we need to kick off an event by calculating a frequency
    * @param windowDuration this is the window duration.
    * @param thersholdBreachCount the number of calculation that breached the threshold for our window.
    * @param microBatchDuration this is a misnomer as such, but better to have it here. It is the rate at which we sample from Kafka,
    * and the windowIncrement has to be a multiple of it, we are keeping it at a multiple of 1.
    * @return weather or not the frequency calculated means an event has to be fired off.
    */
   public static boolean frequencyOfBreachAboveThreshold(long windowDuration, long thersholdBreachCount, long microBatchDuration, MetricTagType metricType) {
      //So we sample every 5 seconds.
      //10 minutes == 600 seconds.
      //600 / 5 = 120
      //So, if we therefore have 120 [CPU load > 95%] in a 10 minute window OR more we should trigger a notification.
      assert windowDuration!=0;
      assert microBatchDuration!=0;
      
      long minFrequency = windowDuration / microBatchDuration;
      LOG.debug(metricType+"::calculated breach threshold Count was::["+minFrequency+"]"); 
      if(thersholdBreachCount >= minFrequency) {
         return true;
      }                 
      return false;
   }
   
   /**
    * Parses the string for datacenterId, zoneId.
    * @param input the key to the tuple.
    * @return an array that is ordered according to datacenter, zone.
    */
   public static String[] stripOutDatacenterEtAl(String input) {
      String stripedOfDoubleQuotes = input.replace("\"", "");
      String[] splitKey = null;
      if(stripedOfDoubleQuotes == null ||
            stripedOfDoubleQuotes.length() == 0) {
         LOG.error("Stripping key of double quotes faile!!");
         //Exception to be thrown when exception handling defect: UHA-2795
      } else {
         splitKey = stripedOfDoubleQuotes.split(":");
         if(splitKey.length !=6) {
            LOG.error("Spliting the key failed to get expected number of tokens::[6]. Actual was::["+splitKey.length+"]");
            //Exception to be thrown when exception handling defect: UHA-2795 is addressed
         }
      }
      return splitKey;
   }
   
   /**
    * It parses out the VM identifier, which is part of the overall key. The
    * primary key being the datacenter, zone, and service. The 2 will constitute
    * the composite key for this class of collector.
    * Example tuple from the JSON:
    * {"timestamp": 1446041354, "value": 0.0, "tags": {"path": "servers.kafka02.cpu.cpu0.guest_nice", 
    * VMKey would be equal to [servers.kafka02]
    * @param v2 which represents the 2nd part of the tuple.
    * @return a String representing the key of the VM which is the server DNS name.
    * @throws Exception that indicates that the matches expected was not 1. 
    */
   public static String parseVMKey(String v2) throws Exception {
      //The following must be parameterized.
      String regexPatternForVMKey = "servers\\.\\w+";
      Pattern pattern = Pattern.compile(regexPatternForVMKey);
      Matcher matcher = pattern.matcher(v2);
      String matchResult = null;
      int vmServerKeyMathCount = 0;
      while (matcher.find()) {
         //group(0) denotes the entire pattern...
         matchResult = matcher.group(0);
         //Calculate the frequency based on the number of RTO SLA's
         ++vmServerKeyMathCount;
         LOG.trace("((((((((((((((VMKeyFound was::))))))))))))))["+matchResult+"]");
      } 
      if(vmServerKeyMathCount != 1) {
         String msg = "Unexpected number of Matches found for regexPatternForVMKey::["+regexPatternForVMKey+"] count was:["+vmServerKeyMathCount+"]. Expected exactly 1 match.";
         LOG.error(msg);
         throw new IllegalArgumentException(msg);
      } else {
         return matchResult;
      }
   }
   /**
    * Utility that Parses out the primary key where
    * applicable...i.e. datacenterId, isolationZone, serviceType
    * 
    * @param v1
    * @return
    * @throws Exception
    */
   public static String parseOutKey(String v1) throws Exception {
      assert v1 != null;
      // Split the tuple into 2 (key part and the rest...assumption based on
      // findings is that the current key is null :=)
      String[] parsedTuple = v1.split(",");
      if (parsedTuple.length == 0) {
         String msg = "v1.split(\",\")==>Failed. Length of array was::["
               + parsedTuple.length + "], expected was >1";
         LOG.error(msg);
         // We have no application specific checked exception at the time of
         // writing. sorry!
         throw new IllegalArgumentException(msg);
      }
      String dataCenterKey = null;
      String isolationZone = null;
      String slaType = null;
      for (int i = 0; i < parsedTuple.length; i++) {
         if (parsedTuple[i].toLowerCase().contains("datacenterid")) {
            dataCenterKey = parsedTuple[i];
         }
         if (parsedTuple[i].toLowerCase().contains("zoneid")) {
            isolationZone = parsedTuple[i];
         }
         if (parsedTuple[i].toLowerCase().contains("type")) {
            slaType = parsedTuple[i];
         }
      }
      if (dataCenterKey == null) {
         LOG.error("datacenterId not found in string, defaulting to unknown:["
               + v1 + "]");
         dataCenterKey = "dataCenterId-none";
      }
      if (isolationZone == null) {
         LOG.error("zoneId not found in string, defaulting to unknown:[" + v1
               + "]");
         isolationZone = "zoneId-none";
      }
      if (slaType == null) {
         LOG.error("slaType not found in string, defaulting to unknown:[" + v1
               + "]");
         isolationZone = "serviceName-none";
      }
      //N.B. if we use a colon, ESM fails to send the key, so we delimit with a "-"
      return dataCenterKey + "-" + isolationZone + "-" + slaType;
   }
}
