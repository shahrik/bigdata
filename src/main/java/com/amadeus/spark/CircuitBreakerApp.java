package com.amadeus.spark;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.amadeus.spark.config.CircuitBreakerConfig;

/**
 * Where it all starts.
 * 
 * @author James King, David Santa-Olalla, Kambiz shahri
 *
 */
public class CircuitBreakerApp {

	/**
	 * @param args
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(CircuitBreakerConfig.class);
		KafkaStreamProcessor kafkaStreamProcessor = ctx.getBean(KafkaStreamProcessor.class);
		kafkaStreamProcessor.process();
	}
}