package com.amadeus.spark.utils;

import java.io.Serializable;

import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.codehaus.jackson.map.ObjectMapper;

import scala.Tuple2;

import com.amadeus.spark.model.LogEvent;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;
import com.amadeus.spark.model.LogEventLqs;
import com.amadeus.spark.sla.model.MessageType;

public class SparkOperations implements Serializable {

   private static final long serialVersionUID = 1L;
   private static final ObjectMapper mapper = new ObjectMapper();

   public LogEvent convertJsonToLogEvent(String jsonString) throws Exception {
      String json = jsonString.replaceAll("@", "");
      LogEvent logLine = mapper.readValue(json, LogEvent.class);
      return logLine;
   }

   public LogEventLqs convertJsonToLogEventLqs(String jsonString)
         throws Exception {
      return new LogEventLqs(jsonString);
   }

   public JavaDStream<LogEvent> convertToPojoStream(
         JavaPairDStream<String, String> input) {
      return input.map((tuple) -> convertJsonToLogEvent(tuple._2()));
   }

   public JavaDStream<LogEventLqs> convertToPojoStreamLqs(
         JavaPairDStream<String, String> input) {
      return input.map((tuple) -> convertJsonToLogEventLqs(tuple._2()));
   }

   public JavaDStream<LogEvent> convertToPojoStreamByDuration(
         JavaPairDStream<String, String> input) {
      return input.map(v1 -> convertJsonToLogEvent(v1._2()))
            .filter(
                  logevent -> logevent.getContent()
                        .getkIndexKey_ClientCallDuration() != null);
   }

   public JavaDStream<LogEvent> convertToPojoStreamByMessageType(
         JavaPairDStream<String, String> input, final String messageType) {
      return input.map(v1 -> convertJsonToLogEvent(v1._2())).filter(
            logevent -> {
               String statEventList = logevent.getHeader()
                     .getkIndexKey_StatEventList();
               return statEventList != null
                     && statEventList.equals(messageType);
            });
   }

   public JavaPairDStream<String, Integer> calculateBreachPercentage(
         JavaPairDStream<String, Tuple2<Integer, Integer>> linesTotalsByKeyTuple) {
      return linesTotalsByKeyTuple
            .mapToPair(tupleHostBreachedAndTotals -> {
               int i = 100 * tupleHostBreachedAndTotals._2()._1()
                     / tupleHostBreachedAndTotals._2()._2();
               return new Tuple2<String, Integer>(tupleHostBreachedAndTotals
                     ._1(), i);
            });
   }

   public JavaPairDStream<String, Integer> getTotalLinesGroupedByFields(
         JavaDStream<LogEvent> lines, LogEventKey.LogEventKeyField... keyFields) {
      JavaPairDStream<String, Integer> linesSplitByHost = lines
            .mapToPair(line -> new Tuple2<String, Integer>(
                  new LogEventKey(line).toJsonOnlyIncludeFields(keyFields), 1));
      return linesSplitByHost.reduceByKey((v1, v2) -> v1 + v2);
   }

   public JavaPairDStream<String, Integer> getTotalLinesGroupedByFieldsLqs(
         JavaDStream<LogEventLqs> lines,
         LogEventLqs.LogEventLqsField... keyFields) {
      JavaPairDStream<String, Integer> linesSplitByHost = lines
            .mapToPair(line -> new Tuple2<String, Integer>(
                  new LogEventLqs(line).toJsonOnlyIncludeFields(keyFields), 1));
      return linesSplitByHost.reduceByKey((v1, v2) -> v1 + v2);
   }

   public JavaPairDStream<String, Integer> findSlaBreach(
         JavaPairDStream<String, Integer> breachedByHost,
         final Integer threshold) {
      return breachedByHost
            .filter(tupleHostPercent -> (tupleHostPercent._2() > threshold));
   }

   public JavaPairDStream<String, Integer> findWithinSla(
         JavaPairDStream<String, Integer> breachPercentageByKey,
         int slaResponseTimeThresholdPercent) {
      return breachPercentageByKey.filter(tupleHostPercent -> (tupleHostPercent
            ._2() <= slaResponseTimeThresholdPercent));
   }

   public JavaDStream<LogEventLqs> filterOutLqs(JavaDStream<LogEventLqs> lines) {
      return lines.filter((logEventLqs) -> {
         // sample for filtering in case we need it some day
            if ((logEventLqs.getDatacenterId().equals("toto")))
               return false;
            else
               return true;
         });
   }

   
   public JavaDStream<LogEvent> filterOutCTRLandRTO(JavaDStream<LogEvent> lines) {
      return lines.filter((logEvent) -> {
         if ((logEvent.getHeader().getkIndexKey_StatEventList() != null)
               && (logEvent.getHeader().getkIndexKey_StatEventList()
                     .equals(MessageType.MESSAGE_CTRL.getCode())))
            return false;
         if ((logEvent.getHeader().getkIndexKey_StatEventList() != null)
               && (logEvent.getHeader().getkIndexKey_StatEventList()
                     .equals(MessageType.MESSAGE_RTO.getCode())))
            return false;
         return true;
      });
   }

   public JavaDStream<LogEvent> filterByMessageType(
         JavaDStream<LogEvent> lines, MessageType messageType) {
      return lines.filter(logEvent -> {
         if ((logEvent.getHeader().getkIndexKey_StatEventList() != null)
               && (logEvent.getHeader().getkIndexKey_StatEventList()
                     .equals(messageType.getCode()))) {
            return true;
         }
         return false;
      });
   }

   public JavaPairDStream<String, Double> getAverageDurationByKey(
         JavaDStream<LogEvent> lines, LogEventKeyField[] logEventKeysToGroupBy) {
      JavaPairDStream<String, Double> linesSplitByKey = filterOutCTRLandRTO(
            lines).mapToPair(
            (logEventsWithDuration) -> new Tuple2<String, Double>(
                  new LogEventKey(logEventsWithDuration)
                        .toJsonOnlyIncludeFields(logEventKeysToGroupBy),
                  logEventsWithDuration.getContent()
                        .getkIndexKey_ClientCallDuration()));

      JavaPairDStream<String, Tuple2<Double, Integer>> intermediateMap = linesSplitByKey
            .mapValues((duration) -> (new Tuple2<Double, Integer>(duration, 1)));

      JavaPairDStream<String, Tuple2<Double, Integer>> hostTupleTotalDurationTotalTransactions = intermediateMap
            .reduceByKey((v1, v2) -> new Tuple2<Double, Integer>(v1._1()
                  + v2._1(), v1._2() + v2._2()));

      JavaPairDStream<String, Double> keyTotalDuration = hostTupleTotalDurationTotalTransactions
            .mapToPair((tupleTotalDurationAndTransactionNumber) -> {
               double avg = tupleTotalDurationAndTransactionNumber._2()._1()
                     / tupleTotalDurationAndTransactionNumber._2()._2();
               return new Tuple2<String, Double>(
                     tupleTotalDurationAndTransactionNumber._1(), avg);

            });
      return keyTotalDuration;
   }

   public JavaPairDStream<String, Integer> getTotalMessageTypeByKey(
         JavaDStream<LogEvent> lines, final MessageType messageType,
         LogEventKey.LogEventKeyField... groupingLogEventKeyFields) {

      JavaPairDStream<String, Integer> ctrlMessageLinesSplitByKey = lines
            .mapToPair((t) -> {
               boolean isMsgTypeExists = t.getHeader()
                     .getkIndexKey_StatEventList() != null;
               boolean isMsgType = isMsgTypeExists ? t.getHeader()
                     .getkIndexKey_StatEventList()
                     .equals(messageType.getCode()) : false;
               return new Tuple2<String, Integer>(new LogEventKey(t)
                     .toJsonOnlyIncludeFields(groupingLogEventKeyFields),
                     (isMsgType ? 1 : 0));
            });

      JavaPairDStream<String, Integer> ctrlMessageLinesSplitByHostAggregated = ctrlMessageLinesSplitByKey
            .reduceByKey((v1, v2) -> v1 + v2);

      return ctrlMessageLinesSplitByHostAggregated;
   }

   public JavaPairDStream<String, Tuple2<Integer, Integer>> breachedToTotalByKey(
         JavaDStream<LogEvent> lines, final MessageType messageType,
         LogEventKey.LogEventKeyField... keyFields) {
      return lines
            .mapToPair(
                  line -> new Tuple2<>(new LogEventKey(line)
                        .toJsonOnlyIncludeFields(keyFields), new Tuple2<>((line
                        .getHeader().getkIndexKey_StatEventList() != null
                        && line.getHeader().getkIndexKey_StatEventList()
                              .equals(messageType.getCode()) ? 1 : 0), 1)))
            .reduceByKey(
                  (v1, v2) -> new Tuple2<>(v1._1() + v2._1(), v1._2() + v2._2()));
   }
   
}
