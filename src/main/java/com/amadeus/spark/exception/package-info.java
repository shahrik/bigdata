/**
 * Bespoke exception Technical and Business, 
 * so that we can differentiate and not use Runtime or 
 * Exception to re-throw or log.
 * @author kambiz.shahri
 *
 */
package com.amadeus.spark.exception;