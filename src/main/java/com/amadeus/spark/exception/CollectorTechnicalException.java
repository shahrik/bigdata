package com.amadeus.spark.exception;

/**
 * Exception class for collectors.
 * @author kambiz.shahri
 *
 */
public class CollectorTechnicalException extends Exception {

   /**
    * 
    */
   private static final long serialVersionUID = 6371659141758145429L;
   
   /**
    * No-Arg Constructor.
    */
   public CollectorTechnicalException() {
      super();
   }
   /**
    * @param message input message for the exception.
    */
   public CollectorTechnicalException(String message) {
      super(message);
   }
 

}
