package com.amadeus.spark.model;

import java.util.Map;

/**
 * A bean that maps to a logEvent.
 * @author James King, David Santa-Olalla
 *
 */
public class LogEvent {
	
	private Map<String, String> metadata;
	
	private String version;
	private String timestamp;
	private String host;
	private String path;
	
	private LogEventHeader header;
	private LogEventContent content;
	
	
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public LogEventHeader getHeader() {
		return header;
	}
	public void setHeader(LogEventHeader header) {
		this.header = header;
	}
	public LogEventContent getContent() {
		return content;
	}
	public void setContent(LogEventContent content) {
		this.content = content;
	}
}