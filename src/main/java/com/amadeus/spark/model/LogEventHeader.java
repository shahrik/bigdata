package com.amadeus.spark.model;

public class LogEventHeader {
	
	private String kIndexKey_TimeStamp;
	private String kIndexKey_HostName;
	private String kIndexKey_ProtocolLayerList;
	private String kIndexKey_StatEventList;
	private String kIndexKey_MonitEventList; 
	private String kIndexKey_AppID;
	private String kIndexKey_PID;
	
	private String kIndexKey_TrcGroup;
	private String kIndexKey_TrcLevel;
	private String kIndexKey_SrcFile;
	private String kIndexKey_SrcLine;
	private String kIndexKey_ThreadID;
	
	public String getkIndexKey_TimeStamp() {
		return kIndexKey_TimeStamp;
	}
	public void setkIndexKey_TimeStamp(String kIndexKey_TimeStamp) {
		this.kIndexKey_TimeStamp = kIndexKey_TimeStamp;
	}
	public String getkIndexKey_HostName() {
		return kIndexKey_HostName;
	}
	public void setkIndexKey_HostName(String kIndexKey_HostName) {
		this.kIndexKey_HostName = kIndexKey_HostName;
	}
	public String getkIndexKey_ProtocolLayerList() {
		return kIndexKey_ProtocolLayerList;
	}
	public void setkIndexKey_ProtocolLayerList(String kIndexKey_ProtocolLayerList) {
		this.kIndexKey_ProtocolLayerList = kIndexKey_ProtocolLayerList;
	}
	public String getkIndexKey_StatEventList() {
		return kIndexKey_StatEventList;
	}
	public void setkIndexKey_StatEventList(String kIndexKey_StatEventList) {
		this.kIndexKey_StatEventList = kIndexKey_StatEventList;
	}
	public String getkIndexKey_MonitEventList() {
		return kIndexKey_MonitEventList;
	}
	public void setkIndexKey_MonitEventList(String kIndexKey_MonitEventList) {
		this.kIndexKey_MonitEventList = kIndexKey_MonitEventList;
	}
	public String getkIndexKey_AppID() {
		return kIndexKey_AppID;
	}
	public void setkIndexKey_AppID(String kIndexKey_AppID) {
		this.kIndexKey_AppID = kIndexKey_AppID;
	}
	public String getkIndexKey_PID() {
		return kIndexKey_PID;
	}
	public void setkIndexKey_PID(String kIndexKey_PID) {
		this.kIndexKey_PID = kIndexKey_PID;
	}
	public String getkIndexKey_TrcGroup() {
		return kIndexKey_TrcGroup;
	}
	public void setkIndexKey_TrcGroup(String kIndexKey_TrcGroup) {
		this.kIndexKey_TrcGroup = kIndexKey_TrcGroup;
	}
	public String getkIndexKey_TrcLevel() {
		return kIndexKey_TrcLevel;
	}
	public void setkIndexKey_TrcLevel(String kIndexKey_TrcLevel) {
		this.kIndexKey_TrcLevel = kIndexKey_TrcLevel;
	}
	public String getkIndexKey_SrcFile() {
		return kIndexKey_SrcFile;
	}
	public void setkIndexKey_SrcFile(String kIndexKey_SrcFile) {
		this.kIndexKey_SrcFile = kIndexKey_SrcFile;
	}
	public String getkIndexKey_SrcLine() {
		return kIndexKey_SrcLine;
	}
	public void setkIndexKey_SrcLine(String kIndexKey_SrcLine) {
		this.kIndexKey_SrcLine = kIndexKey_SrcLine;
	}
	public String getkIndexKey_ThreadID() {
		return kIndexKey_ThreadID;
	}
	public void setkIndexKey_ThreadID(String kIndexKey_ThreadID) {
		this.kIndexKey_ThreadID = kIndexKey_ThreadID;
	}
}
