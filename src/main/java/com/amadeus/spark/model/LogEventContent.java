package com.amadeus.spark.model;

public class LogEventContent {
	
	private String kIndexKey_ServiceName;
	
	private String kIndexKey_Sap;
	private String kIndexKey_CorrelId;
	private String kIndexKey_OriginConvID;
	private String kIndexKey_TargetedApplication;
    	
	private String kIndexKey_Origin;
	private String kIndexKey_Destination;
	private String kIndexKey_MessageID;
	private String kIndexKey_DcxId;
	private String kIndexKey_DcxTx;
	private Double kIndexKey_ClientCallDuration;
	
	private String kIndexKey_Pod;
	private String kIndexKey_Silo;
	private String kIndexKey_IsolationZone;
	private String kIndexKey_DataCenter;
	
	private String kIndexKey_RejectCode;

	public String getkIndexKey_ServiceName() {
		return kIndexKey_ServiceName;
	}

	public void setkIndexKey_ServiceName(String kIndexKey_ServiceName) {
		this.kIndexKey_ServiceName = kIndexKey_ServiceName;
	}

	public String getkIndexKey_Sap() {
		return kIndexKey_Sap;
	}

	public void setkIndexKey_Sap(String kIndexKey_Sap) {
		this.kIndexKey_Sap = kIndexKey_Sap;
	}

	public String getkIndexKey_CorrelId() {
		return kIndexKey_CorrelId;
	}

	public void setkIndexKey_CorrelId(String kIndexKey_CorrelId) {
		this.kIndexKey_CorrelId = kIndexKey_CorrelId;
	}

	public String getkIndexKey_OriginConvID() {
		return kIndexKey_OriginConvID;
	}

	public void setkIndexKey_OriginConvID(String kIndexKey_OriginConvID) {
		this.kIndexKey_OriginConvID = kIndexKey_OriginConvID;
	}

	public String getkIndexKey_TargetedApplication() {
		return kIndexKey_TargetedApplication;
	}

	public void setkIndexKey_TargetedApplication(
			String kIndexKey_TargetedApplication) {
		this.kIndexKey_TargetedApplication = kIndexKey_TargetedApplication;
	}

	public String getkIndexKey_Origin() {
		return kIndexKey_Origin;
	}

	public void setkIndexKey_Origin(String kIndexKey_Origin) {
		this.kIndexKey_Origin = kIndexKey_Origin;
	}

	public String getkIndexKey_Destination() {
		return kIndexKey_Destination;
	}

	public void setkIndexKey_Destination(String kIndexKey_Destination) {
		this.kIndexKey_Destination = kIndexKey_Destination;
	}

	public String getkIndexKey_MessageID() {
		return kIndexKey_MessageID;
	}

	public void setkIndexKey_MessageID(String kIndexKey_MessageID) {
		this.kIndexKey_MessageID = kIndexKey_MessageID;
	}

	public String getkIndexKey_DcxId() {
		return kIndexKey_DcxId;
	}

	public void setkIndexKey_DcxId(String kIndexKey_DcxId) {
		this.kIndexKey_DcxId = kIndexKey_DcxId;
	}

	public String getkIndexKey_DcxTx() {
		return kIndexKey_DcxTx;
	}

	public void setkIndexKey_DcxTx(String kIndexKey_DcxTx) {
		this.kIndexKey_DcxTx = kIndexKey_DcxTx;
	}

	public Double getkIndexKey_ClientCallDuration() {
		return kIndexKey_ClientCallDuration;
	}

	public void setkIndexKey_ClientCallDuration(Double kIndexKey_ClientCallDuration) {
		this.kIndexKey_ClientCallDuration = kIndexKey_ClientCallDuration;
	}

	public String getkIndexKey_RejectCode() {
		return kIndexKey_RejectCode;
	}

	public void setkIndexKey_RejectCode(String kIndexKey_RejectCode) {
		this.kIndexKey_RejectCode = kIndexKey_RejectCode;
	}

	public String getkIndexKey_Pod() {
		return kIndexKey_Pod;
	}

	public void setkIndexKey_Pod(String kIndexKey_Pod) {
		this.kIndexKey_Pod = kIndexKey_Pod;
	}

	public String getkIndexKey_Silo() {
		return kIndexKey_Silo;
	}

	public void setkIndexKey_Silo(String kIndexKey_Silo) {
		this.kIndexKey_Silo = kIndexKey_Silo;
	}

	public String getkIndexKey_IsolationZone() {
		return kIndexKey_IsolationZone;
	}

	public void setkIndexKey_IsolationZone(String kIndexKey_IsolationZone) {
		this.kIndexKey_IsolationZone = kIndexKey_IsolationZone;
	}

	public String getkIndexKey_DataCenter() {
		return kIndexKey_DataCenter;
	}

	public void setkIndexKey_DataCenter(String kIndexKey_DataCenter) {
		this.kIndexKey_DataCenter = kIndexKey_DataCenter;
	}	
}
