package com.amadeus.spark.model;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonFilter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;


@JsonFilter("logEventKeyFilter")
public class LogEventKey {

   public enum LogEventKeyField {
      DATACENTER("dataCenter"), ISOLATION_ZONE("zoneId"), SILO("siloId"), HOST(
            "hostId"), SERVICE_NAME("serviceName");

      private String fieldName;

      private LogEventKeyField(String fieldName) {
         this.fieldName = fieldName;
      }

      public String getFieldName() {
         return fieldName;
      }

   }

   private final String dataCenter;
   private final String zoneId;
   private final String siloId;
   private final String hostId;
   private final String serviceName;

   private static final String UNKNOWN = "UNKNOWN";
   private static final ObjectMapper mapper = new ObjectMapper();   
   private static final ConcurrentHashMap<String, FilterProvider> mapFilterProviderCache = new ConcurrentHashMap<String, FilterProvider>();

   public LogEventKey(LogEvent logEvent) {
      this(logEvent.getContent().getkIndexKey_DataCenter(), logEvent
            .getContent().getkIndexKey_IsolationZone(), logEvent.getContent()
            .getkIndexKey_Silo(), logEvent.getHeader().getkIndexKey_HostName(),
            logEvent.getContent().getkIndexKey_ServiceName());
   }

   @JsonCreator
   private LogEventKey(@JsonProperty("dataCenter") String dataCenter,
         @JsonProperty("zoneId") String zoneId,
         @JsonProperty("siloId") String siloId,
         @JsonProperty("hostId") String hostId,
         @JsonProperty("serviceName") String serviceName) {
      this.dataCenter = (dataCenter == null) ? UNKNOWN : dataCenter;
      this.zoneId = (zoneId == null) ? UNKNOWN : zoneId;
      this.siloId = (siloId == null) ? UNKNOWN : siloId;
      this.hostId = (hostId == null) ? UNKNOWN : hostId;
      this.serviceName = (serviceName == null) ? UNKNOWN : serviceName;
   }

   public String getDataCenter() {
      return dataCenter;
   }

   public String getZoneId() {
      return zoneId;
   }



   public String getSiloId() {
      return siloId;
   }

   public String getHostId() {
      return hostId;
   }

   public String getServiceName() {
      return serviceName;
   }

   public String toJsonOnlyIncludeFields(LogEventKeyField... keyFields) {
      try {
         FilterProvider filter =lookupFilterProvider(keyFields);
         return mapper.writer(filter).writeValueAsString(this);
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }

   private FilterProvider lookupFilterProvider(LogEventKeyField... keyFields) {
      
      String keyFieldsString = Arrays.toString(keyFields);
      FilterProvider filterProviderCached = mapFilterProviderCache.get(keyFieldsString);
      if (filterProviderCached != null) {
         return filterProviderCached;
      }
      
      String[] fieldNameStrings = Arrays.stream(keyFields)
            .map(LogEventKeyField::getFieldName).collect(Collectors.toList())
            .toArray(new String[0]);
      FilterProvider filter = new SimpleFilterProvider().addFilter(
            "logEventKeyFilter",
            SimpleBeanPropertyFilter.filterOutAllExcept(fieldNameStrings));
      mapFilterProviderCache.put(keyFieldsString, filter);
      return filter;
   }

   public static LogEventKey toObject(String json) {
      LogEventKey logEventKey = null;
      try {
         logEventKey = mapper.readValue(json, LogEventKey.class);
         return logEventKey;
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }

   @Override
   public String toString() {
      return "LogEventKey [dataCenter=" + dataCenter + ", zoneId=" + zoneId
            + ", siloId=" + siloId + ", hostId=" + hostId + ", serviceName="
            + serviceName + "]";
   }
}
