package com.amadeus.spark.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A bean that maps to a logEventLqs.
 * @author Jose Morales
 *
 */
public class LogEventLqs implements Serializable {

   private static final long serialVersionUID = 1L;

   private static final Logger log = LoggerFactory
			.getLogger(LogEventLqs.class);


   public enum LogEventLqsField {
	      DATACENTER("dataCenter"), ISOLATION_ZONE("zoneId"), SILO("siloId"), HOST(
	            "hostId"), SERVICE_NAME("serviceName");

	      private String fieldName;

	      private LogEventLqsField(String fieldName) {
	         this.fieldName = fieldName;
	      }

	      public String getFieldName() {
	         return fieldName;
	      }

	   }
	
	private Map<String, String> metadata;
	private int queueVectorSize;

	private boolean isJsonLQsValidStatistics;
	
 	public boolean isJsonLQsValidStatistics() {
		return isJsonLQsValidStatistics;
	}

	public void setJsonLQsValidStatistics(boolean isJsonLQsValidStatistics) {
		this.isJsonLQsValidStatistics = isJsonLQsValidStatistics;
	}

	public LogEventLqs(LogEventLqs line) {
		this.replicationUnitId = line.getReplicationUnitId();
		this.datacenterId = line.getDatacenterId();
		this.zoneId = line.getZoneId();
	
		this.trcGroup = line.getTrcGroup();
		this.trcLevel = line.getTrcLevel();
		this.srcFile = line.getSrcFile();
		this.threadID = line.getThreadID();
		this.queueId = line.getQueueId();
		this.queueName = line.getQueueName();
		this.lowWatermark = line.getLowWatermark();
		this.tableId = line.getTableId();
		this.highWatermark = line.getHighWatermark();
		this.isExceptionQueue = line.getIsExceptionQueue();
		this.isRecoveryQueue = line.getIsRecoveryQueue();
		this.itemsCount = line.getItemsCount();
		this.instantQueuingRate = line.getInstantQueuingRate();
		this.instantDequeuingRate = line.getInstantDequeuingRate();
		this.dequeuedItemsCount = line.getDequeuedItemsCount();
		this.activeItemsCount = line.getActiveItemsCount();
		this.unlockedItemsCount = line.getUnlockedItemsCount();
		this.dequeuableItemsCount = line.getDequeuableItemsCount();
		this.itemsInDBCount = line.getItemsInDBCount();
		this.averageQueueTimeUndelayed = line.getAverageQueueTimeUndelayed();
		this.averageQueueTimeDelayed = line.getAverageQueueTimeDelayed();
		this.recoveryStatus = line.getRecoveryStatus();
		this.currentRecoveryStateDurationDelayed = line.getCurrentRecoveryStateDurationDelayed();
		this.currentRecoveryStateDurationUndelayed = line.getCurrentRecoveryStateDurationUndelayed();

		this.queueVectorSize = line.getQueueId().size();
		this.isJsonLQsValidStatistics = line.isJsonLQsValidStatistics();
		initLocationParameters();

	}

	private void initLocationParameters()
	{
		if(this.replicationUnitId == null){
			this.replicationUnitId = "EmptyReplicationUnit";
		}
		if(this.datacenterId == null){
			this.datacenterId = "EmptyDatacenterId";
		}
		if(this.zoneId == null){
			this.zoneId = "EmptyZoneId";
		}
		if(this.siloId == null){
			this.siloId = "EmptySiloId";
		}
	}
 	
	/*
	 * We check that the JSON contains the keys we need
	 * and that the size is the same for all of them
	 * */
	private boolean checkJsonIsLqsValidStatistics(JSONObject j){
		try {
			if(j.has("content")){
					JSONObject content = j.getJSONObject("content");
					if(	content.has("kIndex_QueueId") && 
						content.has("kIndex_QueueName") && 
						content.has("kIndex_TableId") && 
						content.has("kIndex_ItemsCount") && 
						content.has("kIndex_ItemsInDBCount")  
						){
							//check the sizes
							int qId      = content.getJSONArray("kIndex_QueueId").length();
							int qName    = content.getJSONArray("kIndex_QueueName").length();
							int tId      = content.getJSONArray("kIndex_TableId").length();
							int iCount   = content.getJSONArray("kIndex_ItemsCount").length();
							int iDbCount = content.getJSONArray("kIndex_ItemsInDBCount").length();
							if (    (qId != qName )||
									(qId != tId )||
									(qId != iCount )||
									(qId != iDbCount )){
								return false;
							}
							else {
								return true;
							}
								
					}
					else {
						return false;
					}
			}
			else{
				return false;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public LogEventLqs(String jsonString) {
		JSONObject j;
		try {
			j = new JSONObject(jsonString);
			
			isJsonLQsValidStatistics = checkJsonIsLqsValidStatistics(j);
			if(!isJsonLQsValidStatistics){
				log.error("JSON recieved in LogEvent is not valid LQS statistics (it must contains labels content.kIndex_QueueId, content.kIndex_QueueName, content.kIndex_TableId, content.kIndex_ItemsCount, content.kIndex_ItemsInDBCount with same length size) : -> "+jsonString);
				return;
			}
			
			if(j.has("DatacenterId")){
				datacenterId = j.getString("DatacenterId");
			}
			if(j.has("ReplicationUnitId")){
				replicationUnitId = j.getString("ReplicationUnitId");
			}
			if(j.has("ZoneId")){
				zoneId = j.getString("ZoneId");
			}
			if(j.has("SiloId")){
				siloId = j.getString("SiloId");
			}
			this.hostName = j.getJSONObject("header").getString("kIndexKey_HostName");

			initLocationParameters();
			
			JSONObject content = j.getJSONObject("content");
			
			String key = null;
			JSONArray ja = null;
			
			
			//we supose we have the same size for all vectors
			queueVectorSize = content.getJSONArray("kIndex_TableId").length();

			key = "kIndex_TableId";
			ja = content.getJSONArray(key);
			for (int i = 0; i < ja.length(); i++) {
				  this.tableId.put(i,ja.getString(i));
				}

			key = "kIndex_QueueName";
			ja = content.getJSONArray(key);
			for (int i = 0; i < ja.length(); i++) {
				  this.queueName.put(i,ja.getString(i));
				}

			key = "kIndex_QueueId";
			ja = content.getJSONArray(key);
			for (int i = 0; i < ja.length(); i++) {
				  this.queueId.put(i,ja.getInt(i));
				}

			key = "kIndex_ItemsCount";
			ja = content.getJSONArray(key);
			for (int i = 0; i < ja.length(); i++) {
				  this.itemsCount.put(i,ja.getInt(i));
				}

			key = "kIndex_ItemsInDBCount";
			ja = content.getJSONArray(key);
			for (int i = 0; i < ja.length(); i++) {
				  this.itemsInDBCount.put(i,ja.getInt(i));
				}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("ERROR decoding json:"+jsonString);
		}

	}

	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getTrcGroup() {
		return trcGroup;
	}
	public void setTrcGroup(String trcGroup) {
		this.trcGroup = trcGroup;
	}
	public String getTrcLevel() {
		return trcLevel;
	}
	public void setTrcLevel(String trcLevel) {
		this.trcLevel = trcLevel;
	}
	public String getSrcFile() {
		return srcFile;
	}
	public void setSrcFile(String srcFile) {
		this.srcFile = srcFile;
	}
	public String getThreadID() {
		return threadID;
	}
	public void setThreadID(String threadID) {
		this.threadID = threadID;
	}

	private String version;
	private String timeStamp;
	private String hostName;

	public String getReplicationUnitId() {
		return replicationUnitId;
	}
	public void setReplicationUnitId(String replicationUnitId) {
		this.replicationUnitId = replicationUnitId;
	}
	public String getDatacenterId() {
		return datacenterId;
	}
	public void setDatacenterId(String datacenterId) {
		this.datacenterId = datacenterId;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public void setSiloId(String siloId) {
		this.siloId = siloId;
	}
	private String replicationUnitId;
	private String datacenterId;
	private String zoneId;
	private String siloId;

	
	
	private String trcGroup;
	private String trcLevel;
	private String srcFile;
	private String threadID;

	public int getQueueVectorSize() {
		return queueVectorSize;
	}

	public void setQueueVectorSize(int queueVectorSize) {
		this.queueVectorSize = queueVectorSize;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSiloId() {
		return siloId;
	}

	public Map<Integer,Integer> getQueueId() {
		return queueId;
	}

	public void setQueueId(HashMap<Integer,Integer> queueId) {
		this.queueId = new HashMap<Integer,Integer>(queueId);
	}

	public Map<Integer,String> getQueueName() {
		return queueName;
	}

	public void setQueueName(HashMap<Integer,String> queueName) {
		this.queueName = new HashMap<Integer,String>(queueName);
	}

	public Map<Integer,Integer> getLowWatermark() {
		return lowWatermark;
	}

	public void setLowWatermark(HashMap<Integer,Integer> lowWatermark) {
		this.lowWatermark = new HashMap<Integer,Integer>(lowWatermark);
	}

	public Map<Integer,String> getTableId() {
		return tableId;
	}

	public void setTableId(HashMap<Integer,String> tableId) {
		this.tableId = new HashMap<Integer,String>(tableId);
	}

	public Map<Integer,Integer> getHighWatermark() {
		return highWatermark;
	}

	public void setHighWatermark(HashMap<Integer,Integer> highWatermark) {
		this.highWatermark = new HashMap<Integer,Integer>(highWatermark);
	}

	public Map<Integer,Integer> getIsExceptionQueue() {
		return isExceptionQueue;
	}

	public void setIsExceptionQueue(HashMap<Integer,Integer> isExceptionQueue) {
		this.isExceptionQueue = new HashMap<Integer,Integer>(isExceptionQueue);
	}

	public Map<Integer,Integer> getIsRecoveryQueue() {
		return isRecoveryQueue;
	}

	public void setIsRecoveryQueue(HashMap<Integer,Integer> isRecoveryQueue) {
		this.isRecoveryQueue = new HashMap<Integer,Integer>(isRecoveryQueue);
	}

	public Map<Integer,Integer> getItemsCount() {
		return itemsCount;
	}

	public void setItemsCount(HashMap<Integer,Integer> itemsCount) {
		this.itemsCount = new HashMap<Integer,Integer>(itemsCount);
	}

	public Map<Integer,Integer> getInstantQueuingRate() {
		return instantQueuingRate;
	}

	public void setInstantQueuingRate(HashMap<Integer,Integer> instantQueuingRate) {
		this.instantQueuingRate = new HashMap<Integer,Integer>(instantQueuingRate);
	}

	public Map<Integer,Integer> getInstantDequeuingRate() {
		return instantDequeuingRate;
	}

	public void setInstantDequeuingRate(HashMap<Integer,Integer> instantDequeuingRate) {
		this.instantDequeuingRate = new HashMap<Integer,Integer>(instantDequeuingRate);
	}

	public Map<Integer,Integer> getDequeuedItemsCount() {
		return dequeuedItemsCount;
	}

	public void setDequeuedItemsCount(HashMap<Integer,Integer> dequeuedItemsCount) {
		this.dequeuedItemsCount = new HashMap<Integer,Integer>(dequeuedItemsCount);
	}

	public Map<Integer,Integer> getActiveItemsCount() {
		return activeItemsCount;
	}

	public void setActiveItemsCount(HashMap<Integer,Integer> activeItemsCount) {
		this.activeItemsCount = new HashMap<Integer,Integer>(activeItemsCount);
	}

	public Map<Integer,Integer> getUnlockedItemsCount() {
		return unlockedItemsCount;
	}

	public void setUnlockedItemsCount(HashMap<Integer,Integer> unlockedItemsCount) {
		this.unlockedItemsCount = new HashMap<Integer,Integer>(unlockedItemsCount);
	}

	public Map<Integer,Integer> getDequeuableItemsCount() {
		return dequeuableItemsCount;
	}

	public void setDequeuableItemsCount(HashMap<Integer,Integer> dequeuableItemsCount) {
		this.dequeuableItemsCount = new HashMap<Integer,Integer>(dequeuableItemsCount);
	}

	public Map<Integer,Integer> getItemsInDBCount() {
		return itemsInDBCount;
	}

	public void setItemsInDBCount(HashMap<Integer,Integer> itemsInDBCount) {
		this.itemsInDBCount = new HashMap<Integer,Integer>(itemsInDBCount);
	}

	public Map<Integer,Integer> getAverageQueueTimeUndelayed() {
		return averageQueueTimeUndelayed;
	}

	public void setAverageQueueTime_Undelayed(HashMap<Integer,Integer> averageQueueTime_Undelayed) {
		this.averageQueueTimeUndelayed = new HashMap<Integer,Integer>(averageQueueTime_Undelayed);
	}

	public Map<Integer,Integer> getAverageQueueTimeDelayed() {
		return averageQueueTimeDelayed;
	}

	public void setAverageQueueTime_Delayed(HashMap<Integer,Integer> averageQueueTime_Delayed) {
		this.averageQueueTimeDelayed = new HashMap<Integer,Integer>(averageQueueTime_Delayed);
	}

	public Map<Integer,Integer> getRecoveryStatus() {
		return recoveryStatus;
	}

	public void setRecoveryStatus(HashMap<Integer,Integer> recoveryStatus) {
		this.recoveryStatus = new HashMap<Integer,Integer>(recoveryStatus);
	}

	public Map<Integer,Integer> getCurrentRecoveryStateDurationDelayed() {
		return currentRecoveryStateDurationDelayed;
	}

	public void setCurrentRecoveryStateDuration_Delayed(HashMap<Integer,Integer> currentRecoveryStateDuration_Delayed) {
		this.currentRecoveryStateDurationDelayed = new HashMap<Integer,Integer>(currentRecoveryStateDuration_Delayed);
	}

	public Map<Integer,Integer> getCurrentRecoveryStateDurationUndelayed() {
		return currentRecoveryStateDurationUndelayed;
	}

	public void setCurrentRecoveryStateDuration_Undelayed(HashMap<Integer,Integer> currentRecoveryStateDuration_Undelayed) {
		this.currentRecoveryStateDurationUndelayed = new HashMap<Integer,Integer>(currentRecoveryStateDuration_Undelayed);
	}

	private Map<Integer,Integer> queueId = new HashMap<Integer,Integer>();
	private Map<Integer,String>  queueName = new HashMap<Integer,String>();
	private Map<Integer,Integer> lowWatermark = new HashMap<Integer,Integer>();
	private Map<Integer,String>  tableId = new HashMap<Integer,String>();
	private Map<Integer,Integer> highWatermark = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> isExceptionQueue = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> isRecoveryQueue = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> itemsCount = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> instantQueuingRate = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> instantDequeuingRate = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> dequeuedItemsCount = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> activeItemsCount = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> unlockedItemsCount = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> dequeuableItemsCount = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> itemsInDBCount = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> averageQueueTimeUndelayed = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> averageQueueTimeDelayed = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> recoveryStatus = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> currentRecoveryStateDurationDelayed = new HashMap<Integer,Integer>();
	private Map<Integer,Integer> currentRecoveryStateDurationUndelayed = new HashMap<Integer,Integer>();
	
	
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	public String toJsonOnlyIncludeFields(LogEventLqsField[] keyFields) {
		return toString();
	}

	public String toString()
	{
		return toJsonString();
	}
	
	public String toJsonString()
	{
		String s =  "{ \"DatacenterId\":\""+datacenterId+
						"\",  \"ReplicationUnitId\":\""+replicationUnitId+
						"\", \"ZoneId\":\""+zoneId+
						"\", \"SiloId\":\""+siloId+
						"\", \"content\": {";
		{
			s += "\"kIndex_QueueName\": [";
			for (int i = 0; i < queueVectorSize; i++) {
				  if(i!=0){
					  s+=",";
				  }
				  s += "\""+queueName.get(i)+"\"";
				}
			s+="],";
		}
		{
			s += "\"kIndex_QueueId\": [";
			for (int i = 0; i < queueVectorSize; i++) {
				  if(i!=0){
					  s+=",";
				  }
				  s += ""+queueId.get(i)+"";
				}
			s+="],";
		}
		{
			s += "\"kIndex_ItemsCount\": [";
			for (int i = 0; i < queueVectorSize; i++) {
				  if(i!=0){
					  s+=",";
				  }
				  s += ""+itemsCount.get(i)+"";
				}
			s+="],";
		}
		{
			s += "\"kIndex_ItemsInDBCount\": [";
			for (int i = 0; i < queueVectorSize; i++) {
				  if(i!=0){
					  s+=",";
				  }
				  s += ""+itemsInDBCount.get(i)+"";
				}
			s+="]";
		}
		
		s += "} }";
		return s;
	}


	public static LogEventLqs toObject(String json) 
	{
		LogEventLqs logEventLqs = null;
		logEventLqs = new LogEventLqs(json);
		return logEventLqs;
	}

}