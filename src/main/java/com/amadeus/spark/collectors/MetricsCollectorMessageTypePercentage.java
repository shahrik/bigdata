package com.amadeus.spark.collectors;

import java.time.Instant;

import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.amadeus.spark.model.LogEvent;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;
import com.amadeus.spark.sla.NotificationService;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagWhat;
import com.amadeus.spark.sla.model.MessageType;
import com.amadeus.spark.utils.SparkOperations;

import scala.Tuple2;

/**
 * @author James King, David Santa-Olalla
 *
 */
public class MetricsCollectorMessageTypePercentage implements
		MetricsCollector<JavaPairDStream<String, String>> {

	@Autowired
	private SparkOperations sparkOperations;

	@Autowired
	@Qualifier("applicationMetricsNotifier")
	private NotificationService notificationService;

	private MessageType messageType;

	private final LogEventKeyField[] groupingFields;
	/**
	 * @param messageType
	 */
	public MetricsCollectorMessageTypePercentage(LogEventKeyField[] groupingFields, MessageType messageType) {
	   this.groupingFields = groupingFields.clone();
		this.messageType = messageType;
	}

	/* (non-Javadoc)
	 * @see com.amadeus.spark.collectors.MetricsCollector#collect(java.lang.Object)
	 */
	@Override
	public void collect(JavaPairDStream<String, String> input) {

		JavaDStream<LogEvent> lines = sparkOperations
				.convertToPojoStream(input);

		JavaPairDStream<String, Integer> totalLinesByKey = sparkOperations
				.getTotalLinesGroupedByFields(lines, groupingFields);

		JavaPairDStream<String, Integer> totalBreachesByKey = sparkOperations
				.getTotalMessageTypeByKey(lines, messageType, groupingFields);

		JavaPairDStream<String, Tuple2<Integer, Integer>> breachedToTotalByKey = totalBreachesByKey
				.join(totalLinesByKey);

		JavaPairDStream<String, Integer> breachPercentageByKey = sparkOperations
				.calculateBreachPercentage(breachedToTotalByKey);

		collectAndNotify(breachPercentageByKey);
	}

	/**
	 * @param slaBreachedByHost
	 */
	private void collectAndNotify(
			JavaPairDStream<String, Integer> slaBreachedByHost) {
		slaBreachedByHost
				.foreachRDD(v1 -> {
					for (Tuple2<String, Integer> averageTuple : v1.collect()) {

						String description = "Message type "
								+ messageType.getMetricTagType()
								+ " breach percentage";
						
						long timestamp = Instant.now().getEpochSecond();
						double average = averageTuple._2();
						LogEventKey logEventKey = LogEventKey.toObject(averageTuple._1());
						notificationService.publishMetric(new AnalysisMetric(
								timestamp, average, MetricTagWhat.METRIC_PERCENTAGE,
								messageType.getMetricTagType(), 
								logEventKey.getDataCenter(), 
								logEventKey.getZoneId(), 
								logEventKey.getSiloId(), 
								logEventKey.getHostId(),
								logEventKey.getServiceName(), description));
					}
					return null;
				});
	}

	public MessageType getMessageType() {
		return messageType;
	}
}