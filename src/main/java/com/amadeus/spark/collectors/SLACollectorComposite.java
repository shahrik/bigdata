package com.amadeus.spark.collectors;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;

import com.amadeus.spark.exception.CollectorTechnicalException;
import com.esotericsoftware.minlog.Log;

/**
 * The composite is a launch point for ALL collectors
 * whose bean name starts with slaCollector.
 * This is Spring at its worst...there is no way of knowing
 * how the beans are injected.
 * @author Kambiz Shahri
 *
 */
public class SLACollectorComposite implements SLACollector<JavaPairDStream<String, String>> {
	
   @Autowired(required=false)
   private List<SLACollector<JavaPairDStream<String, String>>> slaCollectors = new ArrayList<SLACollector<JavaPairDStream<String,String>>>();
	
	@Override
	public void collect(JavaPairDStream<String, String> eventsInput, JavaPairDStream<String, String> systemMetricsInput) {
		for(SLACollector<JavaPairDStream<String, String>> slaCollector: slaCollectors) {
		   if(slaCollector.useThisOrThat()) {
		      try {
               slaCollector.collect(eventsInput, null);
            } catch (CollectorTechnicalException e) {
               Log.error("Failed to collect for collector::["+slaCollector+"]"+e);
            }
		   } else {
		      try {
               slaCollector.collect(null, systemMetricsInput);
            } catch (CollectorTechnicalException e) {
               Log.error("Failed to collect for collector::["+slaCollector+"]"+e);
            }
		   }
			
		}
	}

   /* (non-Javadoc)
    * @see com.amadeus.spark.collectors.SLACollector#useThisOrThat()
    */
   @Override
   public boolean useThisOrThat() {
      return false;
   }	
}