package com.amadeus.spark.collectors;

import java.time.Instant;

import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.amadeus.spark.model.LogEvent;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;
import com.amadeus.spark.sla.NotificationService;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagType;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagWhat;
import com.amadeus.spark.utils.SparkOperations;

import scala.Tuple2;

public class MetricsCollectorResponseTimeBreachPercentage implements
		MetricsCollector<JavaPairDStream<String, String>> {


	@Autowired
	private SparkOperations sparkOperations;

	@Autowired
	@Qualifier("applicationMetricsNotifier")
	private NotificationService notificationService;
	
	private double slaResponseTimeThreshold;
	
	private final LogEventKeyField[] groupingFields;
	
	public MetricsCollectorResponseTimeBreachPercentage(LogEventKeyField[] groupingFields,
			double slaResponseTimeThreshold) {
	   this.groupingFields = groupingFields.clone();
		this.slaResponseTimeThreshold = slaResponseTimeThreshold;
	}

	@Override
	public void collect(JavaPairDStream<String, String> input) {

		JavaDStream<LogEvent> lines = sparkOperations
				.convertToPojoStreamByDuration(input);

		JavaPairDStream<String, Integer> totalLinesByKey = sparkOperations
				.getTotalLinesGroupedByFields(lines, groupingFields);

		JavaPairDStream<String, Integer> totalBreachLinesByKey = findBreachedResponseTime(lines, slaResponseTimeThreshold, groupingFields);

		JavaPairDStream<String, Tuple2<Integer, Integer>> breachedToTotalByKey = totalBreachLinesByKey
				.join(totalLinesByKey);

		JavaPairDStream<String, Integer> breachPercentageByKey = sparkOperations
				.calculateBreachPercentage(breachedToTotalByKey);

		collectAndNotify(breachPercentageByKey);
	}

	private void collectAndNotify(
			JavaPairDStream<String, Integer> slaBreachedByKey) {
		slaBreachedByKey.foreachRDD(v1 -> {
			for (Tuple2<String, Integer> averageTuple : v1.collect()) {
				LogEventKey logEventKey = LogEventKey.toObject(averageTuple._1());
				long timestamp = Instant.now().getEpochSecond();
				double average = averageTuple._2();
				notificationService.publishMetric(new AnalysisMetric(timestamp,
													average, 
													MetricTagWhat.METRIC_PERCENTAGE, 
													MetricTagType.DURATION, 
													logEventKey.getDataCenter(), 
													logEventKey.getZoneId(), 
													logEventKey.getSiloId(), 
													logEventKey.getHostId(), 
													logEventKey.getServiceName(), "Response time breached over " + slaResponseTimeThreshold + " percentage"));
			}
			return null;
		});

	}

	private static JavaPairDStream<String, Integer> findBreachedResponseTime(
			JavaDStream<LogEvent> lines, double threshold, LogEventKeyField[] groupingFields) {
		
		JavaPairDStream<String, Integer> hostToLines = lines
				.mapToPair(t -> {
					String key = new LogEventKey(t).toJsonOnlyIncludeFields(groupingFields);
					int value = t.getContent()
							.getkIndexKey_ClientCallDuration() > threshold ? 1 : 0;
					return new Tuple2<String, Integer>(key, value);
				});

		JavaPairDStream<String, Integer> linesByKey = hostToLines
				.reduceByKey((v1, v2) -> v1 + v2);

		return linesByKey;
	}

}