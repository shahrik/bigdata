package com.amadeus.spark.collectors;

import java.time.Instant;

import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.amadeus.spark.model.LogEvent;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;
import com.amadeus.spark.sla.NotificationService;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.MessageType;
import com.amadeus.spark.utils.SparkOperations;

import scala.Tuple2;

public class MetricsCollectorMessageTypeThroughput implements
		MetricsCollector<JavaPairDStream<String, String>> {

	@Autowired
	private SparkOperations sparkOperations;

	@Autowired
	@Qualifier("applicationMetricsNotifier")
	private NotificationService notificationService;

	private MessageType messageType;

	private final LogEventKeyField[] groupingFields;

   public MetricsCollectorMessageTypeThroughput(LogEventKeyField[] groupingFields, MessageType messageType) {
      this.groupingFields = groupingFields.clone();
      this.messageType = messageType;
   }
   
	@Override
	public void collect(JavaPairDStream<String, String> input) {
		JavaDStream<LogEvent> lines = sparkOperations
				.convertToPojoStream(input);
		
		JavaPairDStream<String, Integer> groupedResult = sparkOperations.getTotalMessageTypeByKey(lines, messageType, groupingFields);

		collectAndNotify(groupedResult);
	}

	private void collectAndNotify(
			JavaPairDStream<String, Integer> slaBreachedByKey) {
		slaBreachedByKey.foreachRDD(hostThroughput -> {
			for (Tuple2<String, Integer> throughputTuple : hostThroughput.collect()) {
				long timestamp = Instant.now().getEpochSecond();
				double average = throughputTuple._2();
				LogEventKey logEventKey = LogEventKey.toObject(throughputTuple._1());
				notificationService.publishMetric(new AnalysisMetric(timestamp,
						average, AnalysisMetric.MetricTagWhat.METRIC_THROUGHPUT,
						messageType.getMetricTagType(), 
						logEventKey.getDataCenter(), 
						logEventKey.getZoneId(), 
						logEventKey.getSiloId(), 
						logEventKey.getHostId(),  
						logEventKey.getServiceName(), "Message throughput"));
			}
			return null;
		});
	}

}
