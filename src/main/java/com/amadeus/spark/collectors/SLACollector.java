/**
 * 
 */
package com.amadeus.spark.collectors;

import com.amadeus.spark.exception.CollectorTechnicalException;

/**
 * This distinguishes meta events collectors from 
 * simple collectors, in that the input will be from the SLA 
 * breach event topic rather than the uha-metrics one.
 * 
 * @author kambiz.shahri
 *
 */
public interface SLACollector <T> {
   void collect(T slaBreachEventInput, T systemMetricsInput) throws CollectorTechnicalException;
   boolean useThisOrThat();
   
}