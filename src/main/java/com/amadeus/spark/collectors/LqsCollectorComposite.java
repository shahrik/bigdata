package com.amadeus.spark.collectors;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The composite is a launch point for ALL collectors.
 * Therefore it is of significance.
 * @author Jose M
 *
 */
public class LqsCollectorComposite implements LqsCollector<JavaPairDStream<String, String>> {
	
   @Autowired(required=false)
   private List<LqsCollector<JavaPairDStream<String, String>>> lqsCollectors = new ArrayList<LqsCollector<JavaPairDStream<String,String>>>();
	
	@Override
	public void collect(JavaPairDStream<String, String> input) {
		for(LqsCollector<JavaPairDStream<String, String>> lqsCollector: lqsCollectors) {
			lqsCollector.collect(input);
		}
	}	
}