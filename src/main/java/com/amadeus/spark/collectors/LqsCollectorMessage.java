package com.amadeus.spark.collectors;

import java.time.Instant;

import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.amadeus.spark.model.LogEventLqs;
import com.amadeus.spark.sla.NotificationService;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.utils.SparkOperations;
import com.esotericsoftware.minlog.Log;

public class LqsCollectorMessage implements
		LqsCollector<JavaPairDStream<String, String>> {

	@Autowired
	private SparkOperations sparkOperations;

	@Autowired
	@Qualifier("lqsNotifier")
	private NotificationService notificationService;	

	
	public LqsCollectorMessage() {
      super();
   }
	
	@Override
	public void collect(JavaPairDStream<String, String> input) {
	
		JavaDStream<LogEventLqs> lines = sparkOperations
				.convertToPojoStreamLqs(input);
		
		collectAndNotify(lines);
	}

	/**
	* @param slaBreachedByHost
	*/
	private void collectAndNotify(JavaDStream<LogEventLqs> lines) 
	{
		lines.foreachRDD(lqsLogEvent -> {
			for (LogEventLqs lqsLog : lqsLogEvent.collect()) {

				//if the object does not contains the expected labels, we ignore it
				if(lqsLog.isJsonLQsValidStatistics()){
					String dataCenter = lqsLog.getDatacenterId();
					String zone = lqsLog.getZoneId();
	
					String replicationUnit = lqsLog.getReplicationUnitId();
					String slotId = lqsLog.getHostName();  /* there is one slot per hostname */  
					String hostname = lqsLog.getHostName();    
	
					for (int i=0; i < lqsLog.getQueueId().size(); i++) {
						
				      long timestamp = Instant.now().getEpochSecond();
					  String tableId = lqsLog.getTableId().get(i);  
				      String queueId = lqsLog.getQueueId().get(i).toString();
				      String queueName = lqsLog.getQueueName().get(i);
	
				      notificationService.publishMetric(AnalysisMetric.buildLQSAnalysisMetric(
				            timestamp, 
				            lqsLog.getItemsCount().get(i), 
				            AnalysisMetric.MetricTagWhat.METRIC_COUNT, 
				            AnalysisMetric.MetricTagType.LQS_ITEM, 
							dataCenter,
							zone,
							replicationUnit,
							hostname,
							slotId,  /* there is one slot per hostname */  
							tableId,  
				            queueId, 
				            queueName, 
				            "Number of items remaining in the queue (ItemsCount)"));
				      
				      notificationService.publishMetric(AnalysisMetric.buildLQSAnalysisMetric(
					            timestamp, 
					            lqsLog.getItemsInDBCount().get(i), 
					            AnalysisMetric.MetricTagWhat.METRIC_COUNT, 
					            AnalysisMetric.MetricTagType.LQS_DBITEM, 
								dataCenter,
								zone,
								replicationUnit,
								hostname,
								slotId,  /* there is one slot per hostname */  
								tableId,  
					            queueId, 
					            queueName, 
					            "Number of items remaining in the DB (ItemsInDBCount)"));
				   }
				}
				else{
					Log.error("lqsLog object is not valid LQS Statistics -> "+lqsLog.toString());
				}

			}
			return null;
		});		
	}

}
