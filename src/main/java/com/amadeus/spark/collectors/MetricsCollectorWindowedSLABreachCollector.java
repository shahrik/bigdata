package com.amadeus.spark.collectors;

import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amadeus.spark.exception.CollectorTechnicalException;
import com.amadeus.spark.functions.FunctionsLibrary;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagType;
import com.esotericsoftware.minlog.Log;

/**
 * This collector is a bit special. 
 * The reason is that it is a meta collector. 
 * What does this mean? it means that this collector operates on an existing
 * Collector/Detector::the RTO one. "I stand on the shoulder of giants" principle.
 * This collector streams as input the output of the Collector/Detector of RTO SLA Breaches as input.
 * Why? because the requirements for UHA-2465 Events for RTO's specifies that if these
 * RTO SLA breaches go on for a configurable (default is 60 seconds) amount of time, an event
 * is to be generated to raise an IR.
 * For the purposes of _this_ story, we need to consume from the events.circuitbreaker
 * topic. hence the fact that the collect signature implements the SLACollector interface
 * which maps to the latter topic in the KafkaStreamProcessor.
 * maps to the latter topic.
 *  
 * @author Kambiz.Shahri
 *
 */
public class MetricsCollectorWindowedSLABreachCollector implements 
SLACollector<JavaPairDStream<String, String>> {

   private static final Logger LOG = LoggerFactory
         .getLogger(MetricsCollectorWindowedSLABreachCollector.class);
	
   private long windowDuration;
   private long windowIncrement;  
   private String streamFilter;
   private long microbatchDuration;
   private MetricTagType metricTagType;
   private double threshHoldValue;
   private String calculationPattern;
   private String rddProcessor;
   private String pairerLabel;
   
   /**
    * Constructor.
    * 
    * @param windowDuration
    * @param windowIncrement
    */
   public MetricsCollectorWindowedSLABreachCollector(long windowDuration,
         long windowIncrement, String streamFilter, long microbatchDuration,
         String metricTagName, String calculationPattern,
         double threshHoldValue, String rddProcessor, String pairerLabel) {
      this.windowDuration = windowDuration;
      this.windowIncrement = windowIncrement;
      this.streamFilter = streamFilter;
      this.microbatchDuration = microbatchDuration;
      this.calculationPattern = calculationPattern;
      this.threshHoldValue = threshHoldValue;
      this.metricTagType = AnalysisMetric.MetricTagType.find(metricTagName);
      this.rddProcessor = rddProcessor;
      this.pairerLabel = pairerLabel;
      // Make sure we know what this MetricTag maps to
      switch (metricTagType) {
      case RTO:
         LOG.trace("AnalysisMetric.MetricTagType:[" + metricTagType + "]");
         break;
      case CTRL:
         LOG.trace("AnalysisMetric.MetricTagType:[" + metricTagType + "]");
         break;
      case CPU_LOAD:
         LOG.trace("AnalysisMetric.MetricTagType:["+metricTagType+"]");
         break;
      case DISK_SPACE:
         LOG.trace("AnalysisMetric.MetricTagType:["+metricTagType+"]");
         break;
      case MEMORY:
         LOG.trace("AnalysisMetric.MetricTagType:["+metricTagType+"]");
         break;
      case NETWORK:
         LOG.trace("AnalysisMetric.MetricTagType:["+metricTagType+"]");
         break;
      default:
         Log.error(
               "MetricsCollectorSLABreach::Unexpected AnalysisMetric.MetricTagType:["
                     + metricTagType + "]");
         break;
      }
   }
   
	/* (non-Javadoc)
	 * @see com.amadeus.spark.collectors.MetricsCollector#collect(java.lang.Object)
	 */
	@Override
	public void collect(JavaPairDStream<String, String> eventsInput,  JavaPairDStream<String, String> systemMetricsInput) throws CollectorTechnicalException {
	   JavaPairDStream<String, String> genericInput = null;
	   LOG.debug("useThisOrThat() was::["+useThisOrThat()+"]");
	   //Use this or that Kafka input stream depending on the collector.
	   if(useThisOrThat()) {
	      genericInput = eventsInput;
	      if(eventsInput==null) {
	         LOG.error("eventsInput was null");
	         throw new CollectorTechnicalException("Expected Kafka input stream eventsInput to the collect() was null");
	      }
	      if(systemMetricsInput!=null) {
            LOG.error("systemMetricsInput was NOT null");
         }	      
	   } else {
	      genericInput = systemMetricsInput;
         if(systemMetricsInput==null) {
            LOG.error("systemMetricsInput was null");
            throw new CollectorTechnicalException("Expected Kafka input stream eventsInput to the collect() was null");
         }
         if(eventsInput!=null) {
            LOG.error("eventsInput was NOT null");
         }
	   }
	   //Configure collector now
	   LOG.info(metricTagType+"::Filtering...defn.");
      JavaPairDStream<String, String> filteredStream = genericInput.filter(new FunctionsLibrary.SystemMetricsStreamFilter(streamFilter));

      LOG.info(metricTagType+"::Lines Filtered...defn.");
      JavaDStream<String> lines = filteredStream
            .map(new FunctionsLibrary.LinesFunction());
      
      LOG.info(metricTagType+"::Keying...defn.");
      JavaPairDStream<String, String> jpd = lines.mapToPair(FunctionsLibrary.pairerFactory(pairerLabel));
      
      LOG.info(metricTagType+"::Grouping by Key & Window:["+windowDuration+"] Increment:["+windowIncrement+"]...defn.");
      Duration winDuration = new Duration(windowDuration);
      Duration incrementDuration = new Duration(windowIncrement);
      JavaPairDStream<String, Iterable<String>> grpedJpd = jpd.groupByKeyAndWindow(winDuration, incrementDuration);
 
      LOG.info(metricTagType+"::ForeachRDD...defn.");
      FunctionsLibrary.setMicrobatchDurationInMilliSeconds(microbatchDuration);
      grpedJpd.foreachRDD(FunctionsLibrary.rddProcessorFactory(
            rddProcessor, calculationPattern,
            threshHoldValue, windowDuration, windowIncrement, metricTagType));	   
	}

   @Override
   public boolean useThisOrThat() {
      switch (metricTagType) {
      case RTO:
      case CTRL:
         return true;//==this
      case CPU_LOAD:
      case DISK_SPACE:
      case MEMORY:
      case NETWORK:
         return false;//==that
      default:
         Log.error(
               "MetricsCollectorSLABreach::Unexpected AnalysisMetric.MetricTagType:["
                     + metricTagType + "]");
         break;
      }
      return false;
   }
	
}
