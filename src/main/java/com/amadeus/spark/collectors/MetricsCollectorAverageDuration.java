package com.amadeus.spark.collectors;

import java.time.Instant;

import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.amadeus.spark.model.LogEvent;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;
import com.amadeus.spark.sla.NotificationService;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.utils.SparkOperations;

import scala.Tuple2;

public class MetricsCollectorAverageDuration implements
		MetricsCollector<JavaPairDStream<String, String>> {

	@Autowired
	private SparkOperations sparkOperations;

	@Autowired
	@Qualifier("applicationMetricsNotifier")
	private NotificationService notificationService;
		
	private final LogEventKeyField[] groupingFields;
	
	public MetricsCollectorAverageDuration(LogEventKeyField[] groupingFields) {
	   this.groupingFields = groupingFields.clone();
   }

	@Override
   public void collect(JavaPairDStream<String, String> input) {
		JavaDStream<LogEvent> lines = sparkOperations
				.convertToPojoStream(input);

		JavaPairDStream<String, Double> averageDurationByKey = sparkOperations
				.getAverageDurationByKey(lines, groupingFields);

		collectAndNotify(averageDurationByKey);
	}
/**
* @param slaBreachedByKey
*/
	private void collectAndNotify(
			JavaPairDStream<String, Double> slaBreachedByKey) {
		slaBreachedByKey.foreachRDD(hostAndAverageDuration -> {
			for (Tuple2<String, Double> averageTuple : hostAndAverageDuration.collect()) {
				long timestamp = Instant.now().getEpochSecond();
				double average = averageTuple._2();
				LogEventKey logEventKey = LogEventKey.toObject(averageTuple._1());
				notificationService.publishMetric(new AnalysisMetric(timestamp,
						average, AnalysisMetric.MetricTagWhat.METRIC_AVERAGE,
						AnalysisMetric.MetricTagType.DURATION, 
						logEventKey.getDataCenter(), 
						logEventKey.getZoneId(), 
						logEventKey.getSiloId(),
						logEventKey.getHostId(),
						logEventKey.getServiceName(), "Average response time"));
			}
			return null;
		});
	}

}
