package com.amadeus.spark.collectors;

/**
 * Metrics collector interface.
 * @author Jose M
 * 
 * @param <T>
 */
public interface LqsCollector<T> {
	void collect(T input);

}
