package com.amadeus.spark.collectors;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The composite is a launch point for ALL collectors.
 * Therefore it is of significance.
 * @author David Santa-Olalla
 *
 */
public class MetricsCollectorComposite implements MetricsCollector<JavaPairDStream<String, String>> {
	
   @Autowired(required=false)
   private List<MetricsCollector<JavaPairDStream<String, String>>> metricsCollectors = new ArrayList<MetricsCollector<JavaPairDStream<String,String>>>();
	
	@Override
	public void collect(JavaPairDStream<String, String> input) {
		for(MetricsCollector<JavaPairDStream<String, String>> metricsCollector: metricsCollectors) {
			metricsCollector.collect(input);
		}
	}	
}