package com.amadeus.spark.collectors;

/**
 * Metrics collector interface.
 * @author James King, David Santa-Olalla
 * 
 * @param <T>
 */
public interface MetricsCollector<T> {
	void collect(T input);

}
