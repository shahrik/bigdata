package com.amadeus.spark.detectors;

import java.time.Instant;

import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;

import com.amadeus.spark.model.LogEvent;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.AnalysisMetric.MetricTagType;
import com.amadeus.spark.utils.SparkOperations;

import scala.Tuple2;

/**
 * @author James King, David Santa-Olalla
 * RTO detection.
 *
 */
public class ResponsetimeAnomalyDetection extends SlaBreachDetectorBase<JavaPairDStream<String, String>> {

	@Autowired
	private SparkOperations sparkOperations;

	private double slaResponseTimeThreshold;
	private int slaResponseTimeThresholdPercent;

	private final LogEventKeyField[] groupingFields;
	/**
	 * Constructor.
	 * @param slaResponseTimeThreshold
	 * @param slaResponseTimeThresholdPercent
	 */
	public ResponsetimeAnomalyDetection(LogEventKeyField[] groupingFields, double slaResponseTimeThreshold,
			int slaResponseTimeThresholdPercent) {
	        this.groupingFields = groupingFields.clone();
		this.slaResponseTimeThreshold = slaResponseTimeThreshold;
		this.slaResponseTimeThresholdPercent = slaResponseTimeThresholdPercent;
	}

	/* (non-Javadoc)
	 * @see com.amadeus.spark.detectors.SlaBreachDetector#detect(java.lang.Object)
	 */
	@Override
	public void detect(JavaPairDStream<String, String> input) {

		JavaDStream<LogEvent> lines = sparkOperations
				.convertToPojoStreamByDuration(input);

		JavaPairDStream<String, Integer> totalLinesByKey = sparkOperations
				.getTotalLinesGroupedByFields(lines, groupingFields);

		findSlaEvents(lines, totalLinesByKey, AnalysisMetric.MetricTagSla.SLA_KO);
		
		findSlaEvents(lines, totalLinesByKey, AnalysisMetric.MetricTagSla.SLA_OK);
		
	}
	
	/**
	 * @param lines
	 * @param totalLinesByKey
	 * @param slaType
	 */
	private void findSlaEvents(JavaDStream<LogEvent> lines, JavaPairDStream<String, Integer> totalLinesByKey, AnalysisMetric.MetricTagSla slaType) {
	   
	   JavaPairDStream<String, Integer> totalSlaEventLinesByHost = null;
	   
	   if(AnalysisMetric.MetricTagSla.SLA_OK == slaType) {
	      totalSlaEventLinesByHost = findSlaOkResponseTime(lines, slaResponseTimeThreshold, groupingFields);
	   } else {//KO
	      totalSlaEventLinesByHost = findBreachedResponseTime(lines, slaResponseTimeThreshold, groupingFields);
	   }
	   
      JavaPairDStream<String, Tuple2<Integer, Integer>> breachedToTotalByKey = totalSlaEventLinesByHost
            .join(totalLinesByKey);

      JavaPairDStream<String, Integer> breachPercentageByKey = sparkOperations
            .calculateBreachPercentage(breachedToTotalByKey);

      
      JavaPairDStream<String, Integer> slaEventByKey = sparkOperations
            .findSlaBreach(breachPercentageByKey, slaResponseTimeThresholdPercent);
      collectAndNotify(slaEventByKey, slaType);
      
      slaEventByKey.print();
	}

	/**
	 * @param slaMetricsByHost
	 * @param sla
	 */
	private void collectAndNotify(
			JavaPairDStream<String, Integer> slaMetricsByHost, AnalysisMetric.MetricTagSla sla) {
	   
		slaMetricsByHost
				.foreachRDD(v1 -> {
					for (Tuple2<String, Integer> tupleKeyPercentage : v1.collect()) {
						LogEventKey logEventKey = LogEventKey.toObject(tupleKeyPercentage._1());
						
						String message = "More than 5% of the messages had a response time over 120ms";
					   if(AnalysisMetric.MetricTagSla.SLA_OK == sla) {
					      message = "More than 5% of the messages had a response time less than or equals to 120ms";
					   }
					   
						notifyOfSlaBreach(new AnalysisMetric(
								Instant.now().toEpochMilli(),
								tupleKeyPercentage._2(), 
								AnalysisMetric.MetricTagWhat.SLA, MetricTagType.DURATION, sla,
								logEventKey.getDataCenter(),
								logEventKey.getZoneId(),
								logEventKey.getSiloId(),
								logEventKey.getHostId(),
								logEventKey.getServiceName(), message
						));
					}
					return null;
				});
	}

	private static JavaPairDStream<String, Integer> findBreachedResponseTime(
			JavaDStream<LogEvent> lines, double slaResponseTimeThreshold, LogEventKeyField[] grouping) {
		
		JavaDStream<LogEvent> responsTimeBreached = lines
				.filter(v1 -> v1.getContent().getkIndexKey_ClientCallDuration() > slaResponseTimeThreshold);
		
		JavaPairDStream<String, Integer> keyToLines = responsTimeBreached
				.mapToPair(logEventDelayed -> {
					String key = new LogEventKey(logEventDelayed).toJsonOnlyIncludeFields(grouping);
					return new Tuple2<String, Integer>(key, 1);
				});
		
		JavaPairDStream<String, Integer> linesByHost = keyToLines
				.reduceByKey((v1, v2) -> v1 + v2);
		
		return linesByHost;
	}
	
	/**
	 * This is the flip-side of finding the breach. 
	 * It pairs the SLA OK with the host from which it came from. 
	 * @param lines
	 * @param slaResponseTimeThreshold
	 * @return
	 */
	private static JavaPairDStream<String, Integer> findSlaOkResponseTime(
         JavaDStream<LogEvent> lines, double slaResponseTimeThreshold, LogEventKeyField[] grouping) {
      
      JavaDStream<LogEvent> responsTimeBreached = lines
            .filter(v1 -> v1.getContent().getkIndexKey_ClientCallDuration() <= slaResponseTimeThreshold);
      
      JavaPairDStream<String, Integer> keyToLines = responsTimeBreached
            .mapToPair(logEventDelayed -> {
               String key = new LogEventKey(logEventDelayed).toJsonOnlyIncludeFields(grouping);
               return new Tuple2<String, Integer>(key, 1);
            });
      
      JavaPairDStream<String, Integer> linesByHost = keyToLines
            .reduceByKey((v1, v2) -> v1 + v2);
      
      return linesByHost;
   }
}