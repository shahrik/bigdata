package com.amadeus.spark.detectors;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The composite that initiates all the detectors with the input 
 * from uha-metrics topic presumably.
 * @author James.King
 *
 */
public class SlaBreachDetectorComposite implements SlaBreachDetector<JavaPairDStream<String, String>>{

	@Autowired(required=false)
	private List<SlaBreachDetector<JavaPairDStream<String, String>>> detectors = new ArrayList<SlaBreachDetector<JavaPairDStream<String,String>>>();

	/* (non-Javadoc)
	 * @see com.amadeus.spark.detectors.SlaBreachDetector#detect(java.lang.Object)
	 */
	@Override
	public void detect(JavaPairDStream<String, String> input) {
		for (SlaBreachDetector<JavaPairDStream<String, String>> detector : detectors) {
			detector.detect(input);
		}
	}
}
