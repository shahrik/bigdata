package com.amadeus.spark.detectors;

public interface SlaBreachDetector<T> {
	void detect(T input);
}
