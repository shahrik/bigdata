package com.amadeus.spark.detectors;

import java.time.Instant;

import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.springframework.beans.factory.annotation.Autowired;

import scala.Tuple2;

import com.amadeus.spark.model.LogEvent;
import com.amadeus.spark.model.LogEventKey;
import com.amadeus.spark.model.LogEventKey.LogEventKeyField;
import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.MessageType;
import com.amadeus.spark.utils.SparkOperations;

public class MessageTypeAnomalyDetection extends
		SlaBreachDetectorBase<JavaPairDStream<String, String>> {

	protected static final int SLA_PERCENT_THRESHOLD = 5;

	protected MessageType messageType;

	private final LogEventKeyField[] groupingFields;

	/**
	 * @param messageType
	 */
        public MessageTypeAnomalyDetection(LogEventKeyField[] groupingFields, MessageType messageType) {
	   this.groupingFields = groupingFields.clone();
		this.messageType = messageType;
	}

	@Autowired
	private SparkOperations sparkOperations;

	@Override
	public void detect(JavaPairDStream<String, String> input) {

		JavaDStream<LogEvent> lines = sparkOperations
				.convertToPojoStream(input);


		JavaPairDStream<String, Tuple2<Integer, Integer>> breachedToTotalByKey = sparkOperations
				.breachedToTotalByKey(lines, messageType, groupingFields);


		JavaPairDStream<String, Integer> breachPercentageByKey = sparkOperations
				.calculateBreachPercentage(breachedToTotalByKey);

		
		JavaPairDStream<String, Integer> slaBreachedByKey = sparkOperations
				.findSlaBreach(breachPercentageByKey, SLA_PERCENT_THRESHOLD);
      collectAndNotify(slaBreachedByKey, AnalysisMetric.MetricTagSla.SLA_KO);
      
      
      JavaPairDStream<String, Integer> withinSlaByKey = sparkOperations
            .findWithinSla(breachPercentageByKey, SLA_PERCENT_THRESHOLD);
      collectAndNotify(withinSlaByKey, AnalysisMetric.MetricTagSla.SLA_OK);
	}

	/**
	 * @param slaMetricsByHost
	 * @param sla
	 */
	private void collectAndNotify(
			JavaPairDStream<String, Integer> slaMetricsByHost, AnalysisMetric.MetricTagSla sla) {
		slaMetricsByHost
				.foreachRDD(hostAndPercentageSlaBreached -> {
					for (Tuple2<String, Integer> tupleHostPercentage : hostAndPercentageSlaBreached
							.collect()) {
						long timestamp = Instant.now().toEpochMilli();
						int percentage = tupleHostPercentage._2();
						LogEventKey logEventKey = LogEventKey.toObject(tupleHostPercentage._1());
						
						String message = "More than 5% of the messages had a message type of "
                                             + messageType.getMetricTagType();
                  if(AnalysisMetric.MetricTagSla.SLA_OK == sla) {
                     message = "Less than or exactly 5% of the messages had a message type of "
                                              + messageType.getMetricTagType();
                  }
						
						notifyOfSlaBreach(new AnalysisMetric(timestamp,
								percentage, 
								AnalysisMetric.MetricTagWhat.SLA, messageType.getMetricTagType(), sla,
								logEventKey.getDataCenter(),
								logEventKey.getZoneId(),
								logEventKey.getSiloId(),
								logEventKey.getHostId(), 
								logEventKey.getServiceName(), message
						));
					}
					return null;
				});
	}

	/**
	 * @return
	 */
	public MessageType getMessageType() {
		return messageType;
	}

}