package com.amadeus.spark.detectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.amadeus.spark.sla.NotificationService;
import com.amadeus.spark.sla.model.EventMetric;

/**
 * Abstract base class, for the concrete implementation of the 
 * SLABreachDetector interface.
 * This pattern of an abstract implementing an interface is to 
 * provide a concrete implementation for ALL subclasses to be forced
 * to use the kafkaNotificationServices, which in effect is a broadcast
 * mechanism.
 * 
 * @author James.King
 *
 * @param <T>
 */
public abstract class SlaBreachDetectorBase<T> implements SlaBreachDetector<T> {

	@Autowired
	@Qualifier("kafkaNotificationServices")
	private NotificationService slaBreachNotificationService;

	/**
	 * @param slaBreach
	 * @throws Exception
	 */
	protected void notifyOfSlaBreach(EventMetric slaBreach) throws Exception {
		slaBreachNotificationService.publishMetric(slaBreach);
	}
	
}