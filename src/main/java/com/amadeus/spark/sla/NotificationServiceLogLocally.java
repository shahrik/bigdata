package com.amadeus.spark.sla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.amadeus.spark.sla.model.EventMetric;

@Service
public class NotificationServiceLogLocally implements NotificationService {
	
	private static final Logger log = LoggerFactory
			.getLogger(NotificationServiceLogLocally.class);

	public boolean publishMetric(EventMetric slaBreach) {
		try {
			log.info(slaBreach.toJson());
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return true;
	}
}