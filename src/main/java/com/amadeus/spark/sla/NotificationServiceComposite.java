package com.amadeus.spark.sla;

import java.util.List;

import com.amadeus.spark.sla.model.EventMetric;

/**
 * A composition of a number of notification services that basically 
 * are mapped to different topics in the CirtuitBreakerConfig.
 * Okay so what does this do, this composite is basically
 * acting as a broadcaster to all its notification services.
 * The same notification is routed to more than one destination here.
 * The notification services in the constructor are initialized in the 
 * CirtuitBreakerConfig.
 */
public class NotificationServiceComposite implements NotificationService {
	
	private List<NotificationService> notificationServices;
	
   /**
    * Constructor.
    * @param notificationServices
    */
   public NotificationServiceComposite(
         List<NotificationService> notificationServices) {
      this.notificationServices = notificationServices;
   }

   /* (non-Javadoc)
    * @see com.amadeus.spark.sla.NotificationService#publishMetric(com.amadeus.spark.sla.model.EventMetric)
    */
   @Override
	public boolean publishMetric(EventMetric slaBreach) throws Exception {
		for(NotificationService notificationService: notificationServices) {
			notificationService.publishMetric(slaBreach);
		}
		return true;
	}

	/**
	 * @return
	 */
	public List<NotificationService> getNotificationServices() {
		return notificationServices;
	}

	/**
	 * @param notificationServices
	 */
	public void setNotificationServices(
			List<NotificationService> notificationServices) {
		this.notificationServices = notificationServices;
	}
}