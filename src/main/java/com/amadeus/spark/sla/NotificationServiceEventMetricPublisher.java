package com.amadeus.spark.sla;

import java.io.IOException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.EventMetric;
import com.amadeus.spark.sla.model.EventState;

/**
 * Publishes SLA breaches to the event state manager.
 * The publish publishMetric() requires an EventMetric
 * and it has to be of a specific type, otherwise it will not 
 * publish.
 * 
 * @author James King, David Santa-Olalla, Kambiz Shahri
 *
 */
public class NotificationServiceEventMetricPublisher implements
		NotificationService {

	private final KafkaProducer<String, String> kafkaProducer;

	/**
	 * @return
	 */
	public KafkaProducer<String, String> getKafkaProducer() {
      return kafkaProducer;
   }

   /**
    * @return
    */
   public String getKafkaTopic() {
      return kafkaTopic;
   }

   private final String kafkaTopic;

	private static final Logger log = LoggerFactory
			.getLogger(NotificationServiceEventMetricPublisher.class
					.getName());

	/**
	 * Constructor.
	 * @param kafkaProducer
	 * @param kafkaTopic
	 */
	public NotificationServiceEventMetricPublisher(
			KafkaProducer<String, String> kafkaProducer, String kafkaTopic) {
		this.kafkaProducer = kafkaProducer;
		this.kafkaTopic = kafkaTopic;
	}

	/* (non-Javadoc)
	 * @see com.amadeus.spark.sla.NotificationService#publishMetric(com.amadeus.spark.sla.model.EventMetric)
	 */
	public boolean publishMetric(EventMetric slaBreach) throws JsonGenerationException, JsonMappingException, IOException {
	   
	   if (slaBreach.getClass().isAssignableFrom(AnalysisMetric.class)) {
	      AnalysisMetric analysisMetric = (AnalysisMetric) slaBreach;
	      
	      EventState event = new EventState();
         event.setAgent(analysisMetric.getMetricTagWhat().getValue() + "." + analysisMetric.getType().getValue());
         event.setInstance(analysisMetric.getTags());
         event.setKey(analysisMetric.getTags().get("id"));
         event.setTimestamp(System.currentTimeMillis());
	      
	      switch (analysisMetric.getMetricTagSla()) {
            case SLA_KO: event.setState(EventState.State.ERROR);break;
            case SLA_OK: event.setState(EventState.State.OK);break;
            default:          
               log.error("analysisMetric.getMetricTagSla() type not in SLA_KO or SLA_OK. Publish request failed.");
               return false;
	      }
	      
	      kafkaProducer.send(new ProducerRecord<String, String>(kafkaTopic, event.toJson()));
	      log.info("Event transmitted was::["+event.toJson()+"]"); 
	      return true;
	   } else {
	      log.error("slaBreach.getClass().isAssignableFrom(AnalysisMetric.class) returned false. Publish request failed.");
	      return false;
	   }
	}
}
