package com.amadeus.spark.sla;

import java.io.IOException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amadeus.spark.sla.model.AnalysisMetric;
import com.amadeus.spark.sla.model.EventMetric;

/**
 * Actually does the publishing to the Kafka topic.
 * @author James.King
 *
 */
public class NotificationServiceKafkaMetricPublisher implements
		NotificationService {

	private final KafkaProducer<String, String> kafkaProducer;

	private final String kafkaTopic;

	private static final Logger log = LoggerFactory
			.getLogger(NotificationServiceKafkaMetricPublisher.class
					.getName());

	/**
	 * @param kafkaProducer
	 * @param kafkaTopic
	 */
	public NotificationServiceKafkaMetricPublisher(
			KafkaProducer<String, String> kafkaProducer, String kafkaTopic) {
		this.kafkaProducer = kafkaProducer;
		this.kafkaTopic = kafkaTopic;
	}

	/* (non-Javadoc)
	 * @see com.amadeus.spark.sla.NotificationService#publishMetric(com.amadeus.spark.sla.model.EventMetric)
	 */
	public boolean publishMetric(EventMetric slaBreach) throws JsonGenerationException, JsonMappingException, IOException {
	   if (slaBreach.getClass().isAssignableFrom(AnalysisMetric.class)) {
         AnalysisMetric analysisMetric = (AnalysisMetric) slaBreach;
         if(analysisMetric.getMetricTagSla() == AnalysisMetric.MetricTagSla.SLA_KO 
               || analysisMetric.getMetricTagSla() == AnalysisMetric.MetricTagSla.SLA_NON) {
            kafkaProducer.send(new ProducerRecord<String, String>(kafkaTopic,slaBreach.toJson()));
            log.info(slaBreach.toJson());
         }
	   }
		return true;
	}

}
