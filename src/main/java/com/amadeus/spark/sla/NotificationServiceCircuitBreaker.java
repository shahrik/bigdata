package com.amadeus.spark.sla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amadeus.spark.sla.model.EventMetric;

/**
 * This will make an external call to request service shutdown following an sla breach.
 */
public class NotificationServiceCircuitBreaker implements NotificationService {
	
	private static final Logger log = LoggerFactory
			.getLogger(NotificationServiceCircuitBreaker.class
					.getName());
	
	@Override
	public boolean publishMetric(EventMetric slaBreach) throws Exception {
		log.debug(slaBreach.toJson());
		return false;
	}
}
