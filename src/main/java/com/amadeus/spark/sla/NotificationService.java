package com.amadeus.spark.sla;

import com.amadeus.spark.sla.model.EventMetric;


/**
 * This class takes care of notifying to external systems that an SLA has been breached
 */
public interface NotificationService {
	public boolean publishMetric(EventMetric metric) throws Exception;	
}
